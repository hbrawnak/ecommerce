<?php
/**
 * Created by PhpStorm.
 * User: Habib
 * Date: 24-Aug-15
 * Time: 2:05 PM
 */

class Checkout_Model extends CI_Model
{

    public function check_verified_email($user_email)
    {
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where('user_email', $user_email);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function select_user_by_id($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where('user_id', $user_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    /**
     *
     */
    public function save_order_info()
    {
        $order_data = array();
        $order_data['customer_id'] = $this->session->userdata('user_id');
        $order_data['shipping_id'] = $this->session->userdata('shipping_id');
        $order_data['payment_id'] = $this->session->userdata('payment_id');
        $order_data['order_status'] = 0;
        $order_data['order_total'] = $this->cart->total();
        $order_data['due_date']=$date=date('Y-d-m',strtotime('+3 days'));
        $this->db->insert('tbl_order', $order_data);
        $order_id = $this->db->insert_id();

        //Invoice Update...
        $invoice_no = 'inv-'.date('Y-').$order_id;
        $this->db->set('invoice_no',$invoice_no);
        $this->db->where('order_id',$order_id);
        $this->db->update('tbl_order');

        $order_details_data = array();
        $order_details_data['order_id'] = $order_id;
        $contents = $this->cart->contents();
        foreach ($contents as $product_info) {
            $order_details_data['product_id'] = $product_info['id'];
            $order_details_data['product_name'] = $product_info['name'];
            $order_details_data['product_price'] = $product_info['price'];
            $order_details_data['product_sales_quantity'] = $product_info['qty'];
            $this->db->insert('tbl_order_details', $order_details_data);
        }
        $sql = "UPDATE tbl_product, tbl_order_details
                SET tbl_product.product_quntity = tbl_product.product_quntity - tbl_order_details.product_sales_quantity
                WHERE tbl_product.product_id = tbl_order_details.product_id
                AND tbl_order_details.order_id = '$order_id'";
        $this->db->query($sql);

    }


} 