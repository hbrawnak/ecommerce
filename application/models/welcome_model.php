<?php
/**
 * Created by PhpStorm.
 * User: Habib
 * Date: 14-Aug-15
 * Time: 7:19 PM
 */

class Welcome_Model extends CI_Model {
    public function save_user_info($data)
    {
        $this->db->insert('tbl_user', $data);
    }
    public function select_all_published_product()
    {
        $this->db->select('tbl_product.*', FALSE);
        $this->db->select('tbl_product_special_rate.*', FALSE);
        $this->db->select('tbl_product_image.*', FALSE);
        $this->db->from('tbl_product');
        $this->db->join('tbl_product_special_rate', 'tbl_product.product_id = tbl_product_special_rate.product_id', 'left');
        $this->db->join('tbl_product_image', 'tbl_product.product_id = tbl_product_image.product_id');
        $this->db->where('tbl_product.publication_status', 0);
        $this->db->where('tbl_product_image.default_image', 0);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function select_product_info_by_id($product_id)
    {
        $this->db->select('tbl_product.*', FALSE);
        $this->db->select('tbl_product_special_rate.product_special_price', FALSE);
        $this->db->select('tbl_product_special_rate.date_start', FALSE);
        $this->db->select('tbl_product_special_rate.date_end', FALSE);
        $this->db->select('tbl_product_image.product_image_name', FALSE);
        $this->db->select('tbl_product_manufacturer.manufacturer_name', FALSE);
        $this->db->from('tbl_product');
        $this->db->join('tbl_product_special_rate', 'tbl_product.product_id = tbl_product_special_rate.product_id', 'left');
        $this->db->join('tbl_product_image', 'tbl_product.product_id = tbl_product_image.product_id', 'left');
        $this->db->join('tbl_product_manufacturer', 'tbl_product.manufacturer_id = tbl_product_manufacturer.manufacturer_id', 'left');
        $this->db->where('tbl_product.product_id', $product_id);
        $this->db->where('tbl_product_image.default_image', 0);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function select_product_image_by_id($product_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_product_image');
        $this->db->where('product_id', $product_id);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function select_all_published_category()
    {
        $sql = "SELECT * FROM tbl_category WHERE publication_status='0'";
        $query_result = $this->db->query($sql);
        $result = $query_result->result();
        return $result;
    }
    public function select_all_published_manufacturer()
    {
        $sql = "SELECT * FROM tbl_product_manufacturer WHERE publication_status='0'";
        $query_result = $this->db->query($sql);
        $result = $query_result->result();
        return $result;
    }
    public function select_all_published_product_by_category_id($category_id)
    {
        $this->db->select('tbl_product.*', FALSE);
        $this->db->select('tbl_product_special_rate.*', FALSE);
        $this->db->select('tbl_product_image.*', FALSE);
        $this->db->from('tbl_product');
        $this->db->join('tbl_product_special_rate', 'tbl_product.product_id = tbl_product_special_rate.product_id', 'left');
        $this->db->join('tbl_product_image', 'tbl_product.product_id = tbl_product_image.product_id');
        $this->db->where('tbl_product.category_id', $category_id);
        $this->db->where('tbl_product.publication_status', 0);
        $this->db->where('tbl_product_image.default_image', 0);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function select_all_published_product_by_manufacturer_id($manufacturer_id)
    {
        $this->db->select('tbl_product.*', FALSE);
        $this->db->select('tbl_product_special_rate.*', FALSE);
        $this->db->select('tbl_product_image.*', FALSE);
        $this->db->from('tbl_product');
        $this->db->join('tbl_product_special_rate', 'tbl_product.product_id = tbl_product_special_rate.product_id', 'left');
        $this->db->join('tbl_product_image', 'tbl_product.product_id = tbl_product_image.product_id');
        $this->db->where('tbl_product.manufacturer_id', $manufacturer_id);
        $this->db->where('tbl_product.publication_status', 0);
        $this->db->where('tbl_product_image.default_image', 0);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
}