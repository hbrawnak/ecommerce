<?php
/**
 * Created by PhpStorm.
 * User: Habib
 * Date: 14-Aug-15
 * Time: 9:24 PM
 */

class Super_Admin_Model extends CI_Model {
    public function save_user_info($data)
    {
        $this->db->insert('tbl_user', $data);
    }
    public function select_all_user($per_page,$offset)
    {
        /*if($offset==NULL)
        {
            $offset=0;
        }
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->LIMIT($offset,$per_page);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;*/

        if($offset==NULL)
        {
            $offset=0;
        }
        $sql = "SELECT * FROM tbl_user LIMIT $offset,$per_page ";
        $query_result = $this->db->query($sql);
        $result = $query_result->result();
        return $result;



    }
    public function inactive_user_by_id($user_id)
    {
        $this->db->set('user_status',1);
        $this->db->where('user_id',$user_id);
        $this->db->update('tbl_user');
    }
    public function active_user_by_id($user_id)
    {
        $this->db->set('user_status',0);
        $this->db->where('user_id',$user_id);
        $this->db->update('tbl_user');
    }
    public function delete_user_by_id($user_id)
    {
        $this->db->where('user_id',$user_id);
        $this->db->delete('tbl_user');
    }
    public function select_user_info_by_id($user_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where('user_id',$user_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function update_user_info($data,$user_id)
    {
        $this->db->set('user_first_name');
        $this->db->set('user_last_name');
        $this->db->set('user_name');
        $this->db->set('user_email');
        $this->db->set('user_password');
        $this->db->where('user_id',$user_id);
        $this->db->update('tbl_user',$data);
    }
    public function save_manufacturer_info($data)
    {
        $this->db->insert('tbl_product_manufacturer', $data);
    }
    public function select_all_manufacturer()
    {
        $this->db->select('*');
        $this->db->from('tbl_product_manufacturer');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function unpublished_manufacturer_by_id($manufacturer_id)
    {
        $this->db->set('publication_status',1);
        $this->db->where('manufacturer_id',$manufacturer_id);
        $this->db->update('tbl_product_manufacturer');
    }
    public function published_manufacturer_by_id($manufacturer_id)
    {
        $this->db->set('publication_status',0);
        $this->db->where('manufacturer_id',$manufacturer_id);
        $this->db->update('tbl_product_manufacturer');
    }
    public function delete_manufacturer_by_id($manufacturer_id)
    {
        $this->db->where('manufacturer_id',$manufacturer_id);
        $this->db->delete('tbl_product_manufacturer');
    }
    public function select_manufacturer_info_by_id($manufacturer_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_product_manufacturer');
        $this->db->where('manufacturer_id',$manufacturer_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function update_manufacturer_info($data,$manufacturer_id)
    {
        $this->db->set('manufacturer_name');
        $this->db->set('manufacturer_description');
        $this->db->set('publication_status');
        $this->db->where('manufacturer_id',$manufacturer_id);
        $this->db->update('tbl_product_manufacturer',$data);
    }
    public function save_category_info($data)
    {
        $this->db->insert('tbl_category', $data);
    }
    public function select_all_category()
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function unpublished_category_by_id($category_id)
    {
        $this->db->set('publication_status',1);
        $this->db->where('category_id',$category_id);
        $this->db->update('tbl_category');
    }
    public function published_category_by_id($category_id)
    {
        $this->db->set('publication_status',0);
        $this->db->where('category_id',$category_id);
        $this->db->update('tbl_category');
    }
    public function delete_category_by_id($category_id)
    {
        $this->db->where('category_id',$category_id);
        $this->db->delete('tbl_category');
    }
    public function select_category_info_by_id($category_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('category_id',$category_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function update_category_info($data,$category_id)
    {
        $this->db->set('category_name');
        $this->db->set('category_description');
        $this->db->set('publication_status');
        $this->db->where('category_id',$category_id);
        $this->db->update('tbl_category',$data);
    }
    public function select_all_published_manufacturer()
    {
        $this->db->select('*');
        $this->db->from('tbl_product_manufacturer');
        $this->db->where('publication_status',0);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function select_all_published_category()
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('publication_status',0);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function save_product_info($data)
    {
        $this->db->insert('tbl_product', $data);
        $product_id=$this->db->insert_id();
        return $product_id;
    }
    public function save_product_image_info($data)
    {
        $this->db->insert('tbl_product_image', $data);
    }
    public function save_product_special_price($data)
    {
        $this->db->insert('tbl_product_special_rate', $data);
    }
    public function select_all_product()
    {
        $this->db->select('tbl_product.*', FALSE);
        $this->db->select('tbl_product_special_rate.*', FALSE);
        $this->db->select('tbl_product_image.*', FALSE);
        $this->db->from('tbl_product');
        $this->db->join('tbl_product_special_rate', 'tbl_product.product_id = tbl_product_special_rate.product_id', 'left');
        $this->db->join('tbl_product_image', 'tbl_product.product_id = tbl_product_image.product_id');
        $this->db->where('tbl_product_image.default_image', 0);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function unpublished_product_by_id($product_id)
    {
        $this->db->set('publication_status',1);
        $this->db->where('product_id',$product_id);
        $this->db->update('tbl_product');
    }
    public function published_product_by_id($product_id)
    {
        $this->db->set('publication_status',0);
        $this->db->where('product_id',$product_id);
        $this->db->update('tbl_product');
    }
    public function delete_product_by_id($product_id)
    {
        $this->db->where('product_id',$product_id);
        $this->db->delete('tbl_product_image');
        $this->db->delete('tbl_product_special_rate');
        $this->db->delete('tbl_product');
    }
    public function all_orders($per_page,$offset)
    {
        if($offset==NULL)
        {
            $offset=0;
        }
        $sql = "SELECT o.order_id,o.invoice_no,o.order_total,o.order_status,u.user_first_name,u.user_last_name
                FROM tbl_order as o, tbl_user as u
                WHERE o.customer_id=u.user_id LIMIT $offset,$per_page";
        $query_result = $this->db->query($sql);
        $result = $query_result->result();
        return $result;
    }
    public function select_order_by_id($order_id)
    {
        $sql = "SELECT * FROM tbl_order WHERE order_id='$order_id'";
        $query_result = $this->db->query($sql);
        $result = $query_result->row();
        return $result;
    }
    public function delete_order_by_id($order_id)
    {
        $this->db->where('order_id',$order_id);
        $this->db->delete('tbl_order_details');
        $this->db->delete('tbl_order');
    }
    public function select_customer_by_id($customer_id)
    {
        $sql = "SELECT * FROM tbl_user WHERE user_id='$customer_id'";
        $query_result = $this->db->query($sql);
        $result = $query_result->row();
        return $result;
    }
    public function select_shipping_by_id($shipping_id)
    {
        $sql = "SELECT * FROM tbl_shipping WHERE shipping_id='$shipping_id'";
        $query_result = $this->db->query($sql);
        $result = $query_result->row();
        return $result;
    }
    public function order_details_info($order_id)
    {
        $sql = "SELECT * FROM tbl_order_details WHERE order_id='$order_id'";
        $query_result = $this->db->query($sql);
        $result = $query_result->result();
        return $result;
    }
    public function order_search_info($per_page,$offset,$search_text)
    {
        if($offset==NULL)
        {
            $offset=0;
        }
        $sql = "SELECT o.order_id,o.invoice_no,o.order_total,o.order_status,u.user_first_name,u.user_last_name
                FROM tbl_order as o, tbl_user as u
                WHERE o.customer_id=u.user_id
                AND (order_id='$search_text'
                OR invoice_no LIKE '%$search_text%'
                OR user_first_name LIKE '%$search_text%'
                OR user_last_name LIKE '%$search_text%') LIMIT $offset,$per_page";
        $query_result = $this->db->query($sql);
        $result = $query_result->result();
        return $result;
    }
    public function user_search_info($per_page,$offset,$search_text)
    {
        if($offset==NULL)
        {
            $offset=0;
        }
        $sql = "SELECT *
                FROM tbl_user as u
                WHERE u.user_id
                AND (user_id='$search_text'
                OR user_email LIKE '%$search_text%'
                OR user_first_name LIKE '%$search_text%'
                OR user_last_name LIKE '%$search_text%') LIMIT $offset,$per_page";
        $query_result = $this->db->query($sql);
        $result = $query_result->result();
        return $result;

    }
    public function update_product_quantity_by_id($product_id,$data)
    {
        $this->db->set('product_quntity');
        $this->db->where('product_id',$product_id);
        $this->db->update('tbl_product',$data);

        //"UPDATE tbl_product SET product_quntity= product_quntity + $data WHERE product_id= $product_id";
    }

}