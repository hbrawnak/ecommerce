<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function pdf_create($html, $filename='', $stream=TRUE) 
{
    require_once("dompdf/dompdf_config.inc.php");
        
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->render();
        //$Style = new Style();
        //$Style->set_font_size(2);
        //$Style->set_color('red');
        
    if ($stream) {
                $dompdf->stream("Reporte.pdf", array("Attachment" => 0));
        //$dompdf->stream($filename.".pdf");
    } else {
        return $dompdf->output();
    }
}
?>  