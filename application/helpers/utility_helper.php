<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');


if(!function_exists("current_uri")) {
    function current_uri() {
        $ci =& get_instance();
        //$uri = $ci->router->fetch_class()."/".$ci->router->fetch_method();
        $uri = current_url();
        return $uri;
    }
}