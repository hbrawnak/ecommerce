<?php
/**
 * Created by PhpStorm.
 * User: Habib
 * Date: 20-Aug-15
 * Time: 7:14 PM
 */

class Cart extends CI_Controller {
    public function add_to_cart($product_id)
    {
        $product_info = $this->welcome_model->select_product_info_by_id($product_id);
        $qty = $this->input->post('qty', true);
        if($product_info->product_special_price)
        {
            $product_price = $product_info->product_special_price;
        }
        else
        {
            $product_price = $product_info->product_price;
        }
        $data = array(
            'id'      => $product_info->product_id,
            'qty'     => $qty,
            'price'   => $product_price,
            'name'    => $product_info->product_name,
            'image'   => $product_info->product_image_name
        );
        $this->cart->insert($data);
        redirect('cart/show_cart');
    }
    public function update_cart($drowid=NULL)
    {
        $qty = $this->input->post('qty', true);
        $rowid = $this->input->post('rowid', true);
       if($drowid)
       {
           $data = array(
               'rowid' => $drowid,
               'qty'   => 0
           );

           $this->cart->update($data);
       }
        else {
            $data = array(
                'rowid' => $rowid,
                'qty' => $qty
            );

            $this->cart->update($data);
        }
        redirect('cart/show_cart');
    }

    public function show_cart()
    {
        $data=array();
        $data['title']='Cart';
        $data['user_id']=$this->session->userdata('user_id');
        $data['maincontent']=$this->load->view('cart_view',$data,TRUE);
        $this->load->view('master',$data);
    }
} 