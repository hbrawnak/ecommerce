<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Habib
 * Date: 05-Aug-15
 * Time: 10:47 AM
 */
class Welcome extends CI_Controller
{
    public function index()
    {
        $data = array();
        $data['user_id'] = $this->session->userdata('user_id');
        $data['title'] = 'Home';
        $data['all_published_product'] = $this->welcome_model->select_all_published_product();
        $data['maincontent'] = $this->load->view('home_view', $data, TRUE);
        $this->load->view('master', $data);
    }

    public function apparel()
    {
        $data = array();
        $data['title'] = 'Apparel';
        $data['user_id'] = $this->session->userdata('user_id');
        $data['all_published_category'] = $this->welcome_model->select_all_published_category();
        $data['all_published_manufacturer'] = $this->welcome_model->select_all_published_manufacturer();
        $data['all_published_product'] = $this->welcome_model->select_all_published_product();
        $data['maincontent'] = $this->load->view('apparel_view', $data, TRUE);
        $this->load->view('master', $data);
    }
    public function category($category_id)
    {
        $data = array();
        $data['title'] = 'Category';
        $data['user_id'] = $this->session->userdata('user_id');
        $data['all_published_category'] = $this->welcome_model->select_all_published_category();
        $data['all_published_manufacturer'] = $this->welcome_model->select_all_published_manufacturer();
        $data['all_published_product_by_category'] = $this->welcome_model->select_all_published_product_by_category_id($category_id);
        $data['maincontent'] = $this->load->view('category_view', $data, TRUE);
        $this->load->view('master', $data);
    }
    public function manufacturer($manufacturer_id)
    {
        $data = array();
        $data['title'] = 'Brand';
        $data['user_id'] = $this->session->userdata('user_id');
        $data['all_published_category'] = $this->welcome_model->select_all_published_category();
        $data['all_published_manufacturer'] = $this->welcome_model->select_all_published_manufacturer();
        $data['all_published_product_by_manufacturer'] = $this->welcome_model->select_all_published_product_by_manufacturer_id($manufacturer_id);
        $data['maincontent'] = $this->load->view('brand_view', $data, TRUE);
        $this->load->view('master', $data);
    }

    public function details($product_id)
    {
        $data = array();
        $data['title'] = 'Product Details';
        $data['product_info'] = $this->welcome_model->select_product_info_by_id($product_id);
        $data['product_image'] = $this->welcome_model->select_product_image_by_id($product_id);
        $data['all_published_category'] = $this->welcome_model->select_all_published_category();
        $data['all_published_manufacturer'] = $this->welcome_model->select_all_published_manufacturer();
        $data['user_id'] = $this->session->userdata('user_id');
        $data['maincontent'] = $this->load->view('details_view', $data, TRUE);
        $this->load->view('master', $data);
    }

    public function register()
    {
        $data = array();
        $data['title'] = 'Register';
        //$data['user_id'] = $this->session->userdata('user_id');
        $data['maincontent'] = $this->load->view('register_view', $data, TRUE);
        $this->load->view('master', $data);
    }

    public function contact()
    {
        $data = array();
        $data['title'] = 'Contact';
        $data['user_id'] = $this->session->userdata('user_id');
        $data['maincontent'] = $this->load->view('contact_view', $data, TRUE);
        $this->load->view('master', $data);
    }
} 