<?php
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Habib
 * Date: 06-Aug-15
 * Time: 8:04 PM
 */

class Super_Admin extends CI_Controller {

    public function __construct()
    {
        parent:: __construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==NULL)
        {
            redirect('admin_login','refresh');
        }
        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "100%",
                'height' => "200px"

            )
        );
    }
    public function index()
    {
        $data=array();
        $data['admin_content']=$this->load->view('admin/home_view','',true);
        $data['title']='Home';
        $this->load->view('admin/master',$data);
    }

    public function add_user()
    {
        $data=array();
        $data['title']='Add User';
        $data['admin_content']=$this->load->view('admin/add_user',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function save_user()
    {
        $data=array();
        $data['user_first_name']=$this->input->post('user_first_name',true);
        $data['user_last_name']=$this->input->post('user_last_name',true);
        $data['user_name']=$this->input->post('user_name',true);
        $data['user_email']=$this->input->post('user_email',true);
        $data['user_password']=md5($this->input->post('user_password',true));
        $data['mobile_no']=$this->input->post('mobile_no',true);
        $data['address']=$this->input->post('address',true);
        $data['zip_code']=$this->input->post('zip_code',true);
        $data['city']=$this->input->post('city',true);
        $data['country']=$this->input->post('country',true);
        $data['state']=$this->input->post('state',true);
        $data['newsletter']=$this->input->post('newsletter',true);

        $this->super_admin_model->save_user_info($data);
        $sdata=array();
        $sdata['message']="User added successfully !";
        $this->session->set_userdata($sdata);
        redirect('super_admin/add_user');
    }
    public function manage_user()
    {
        $data=array();
        $this->load->library('pagination');

        $config['base_url'] = base_url() .'super_admin/manage_user/';
        $config['total_rows'] = $this->db->count_all('tbl_user');
        $config['per_page'] = 5;
        //$config['full_tag_open'] = '<p>';
       // $config['full_tag_close'] = '</p>';

        $this->pagination->initialize($config);
        $data['all_user']=$this->super_admin_model->select_all_user($config['per_page'], $this->uri->segment(3));
        $data['title']='Manage User';
        $data['admin_content']=$this->load->view('admin/manage_user',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function user_search()
    {
        $search_text = $this->input->post('search_text');

        $data=array();
        $this->load->library('pagination');

        $config['base_url'] = base_url() .'super_admin/manage_user/';
        $config['total_rows'] = $this->db->count_all('tbl_user');
        $config['per_page'] = 5;
        //$config['full_tag_open'] = '<p>';
        // $config['full_tag_close'] = '</p>';

        $this->pagination->initialize($config);
        $data['all_user']=$this->super_admin_model->user_search_info($config['per_page'], $this->uri->segment(3),$search_text);
        $data['title']='Manage User';
        $data['admin_content']=$this->load->view('admin/manage_user',$data,true);
        $this->load->view('admin/master',$data);


    }
    public function inactive_user($user_id)
    {
        $this->super_admin_model->inactive_user_by_id($user_id);
        redirect('super_admin/manage_user');
    }
    public function active_user($user_id)
    {
        $this->super_admin_model->active_user_by_id($user_id);
        redirect('super_admin/manage_user');
    }
    public function delete_user($user_id)
    {
        $this->super_admin_model->delete_user_by_id($user_id);
        redirect('super_admin/manage_user');
    }
    public function edit_user($user_id)
    {
        $data=array();
        $data['user_info']=$this->super_admin_model->select_user_info_by_id($user_id);
        $data['admin_content']=$this->load->view('admin/edit_user',$data,true);
        $data['title']='Edit User';
        $this->load->view('admin/master',$data);
    }
    public function update_user($user_id)
    {
        $data=array();
        $data['user_first_name']=$this->input->post('user_first_name',true);
        $data['user_last_name']=$this->input->post('user_last_name',true);
        $data['user_name']=$this->input->post('user_name',true);
        $data['user_email']=$this->input->post('user_email',true);
        $data['user_password']=md5($this->input->post('user_password',true));

        $this->super_admin_model->update_user_info($data,$user_id);
        $sdata=array();
        $sdata['message']="User information updated successfully !";
        $this->session->set_userdata($sdata);
        redirect('super_admin/manage_user');
    }
    public function add_manufacturer()
    {
        $data=array();
        $data['title']='Add Manufacturer';
        $data['admin_content']=$this->load->view('admin/add_manufacturer',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function save_manufacturer()
    {
        $data=array();
        $data['manufacturer_name']=$this->input->post('manufacturer_name',true);
        $data['manufacturer_description']=$this->input->post('manufacturer_description',true);
        $data['publication_status']=$this->input->post('publication_status',true);

        $this->super_admin_model->save_manufacturer_info($data);
        $sdata=array();
        $sdata['message']="Manufacturer added successfully !";
        $this->session->set_userdata($sdata);
        redirect('super_admin/add_manufacturer');
    }
    public function manage_manufacturer()
    {
        $data=array();
        $data['title']='Manage Manufacturer';
        $data['all_manufacturer']=$this->super_admin_model->select_all_manufacturer();
        $data['admin_content']=$this->load->view('admin/manage_manufacturer',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function unpublished_manufacturer($manufacturer_id)
    {
        $this->super_admin_model->unpublished_manufacturer_by_id($manufacturer_id);
        redirect('super_admin/manage_manufacturer');
    }
    public function published_manufacturer($manufacturer_id)
    {
        $this->super_admin_model->published_manufacturer_by_id($manufacturer_id);
        redirect('super_admin/manage_manufacturer');
    }
    public function delete_manufacturer($manufacturer_id)
    {
        $this->super_admin_model->delete_manufacturer_by_id($manufacturer_id);
        redirect('super_admin/manage_manufacturer');
    }
    public function edit_manufacturer($manufacturer_id)
    {
        $data=array();
        $data['manufacturer_info']=$this->super_admin_model->select_manufacturer_info_by_id($manufacturer_id);
        $data['admin_content']=$this->load->view('admin/edit_manufacturer',$data,true);
        $data['title']='Edit Manufacturer';
        $this->load->view('admin/master',$data);
    }
    public function update_manufacturer($manufacturer_id)
    {
        $data=array();
        $data['manufacturer_name']=$this->input->post('manufacturer_name',true);
        $data['manufacturer_description']=$this->input->post('manufacturer_description',true);
        $data['publication_status']=$this->input->post('publication_status',true);

        $this->super_admin_model->update_manufacturer_info($data,$manufacturer_id);
        $sdata=array();
        $sdata['message']="Manufacturer information updated successfully !";
        $this->session->set_userdata($sdata);
        redirect('super_admin/manage_manufacturer');
    }
    public function add_category()
    {
        $data=array();
        $data['title']='Add Category';
        $data['admin_content']=$this->load->view('admin/add_category',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function save_category()
    {
        $data=array();
        $data['category_name']=$this->input->post('category_name',true);
        $data['category_description']=$this->input->post('category_description',true);
        $data['publication_status']=$this->input->post('publication_status',true);

        $this->super_admin_model->save_category_info($data);
        $sdata=array();
        $sdata['message']="Category added successfully !";
        $this->session->set_userdata($sdata);
        redirect('super_admin/add_category');
    }
    public function manage_category()
    {
        $data=array();
        $data['title']='Manage Category';
        $data['all_category']=$this->super_admin_model->select_all_category();
        $data['admin_content']=$this->load->view('admin/manage_category',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function unpublished_category($category_id)
    {
        $this->super_admin_model->unpublished_category_by_id($category_id);
        redirect('super_admin/manage_category');
    }
    public function published_category($category_id)
    {
        $this->super_admin_model->published_category_by_id($category_id);
        redirect('super_admin/manage_category');
    }
    public function delete_category($category_id)
    {
        $this->super_admin_model->delete_category_by_id($category_id);
        redirect('super_admin/manage_category');
    }
    public function edit_category($category_id)
    {
        $data=array();
        $data['category_info']=$this->super_admin_model->select_category_info_by_id($category_id);
        $data['admin_content']=$this->load->view('admin/edit_category',$data,true);
        $data['title']='Edit Category';
        $this->load->view('admin/master',$data);
    }
    public function update_category($category_id)
    {
        $data=array();
        $data['category_name']=$this->input->post('category_name',true);
        $data['category_description']=$this->input->post('category_description',true);
        $data['publication_status']=$this->input->post('publication_status',true);

        $this->super_admin_model->update_category_info($data,$category_id);
        $sdata=array();
        $sdata['message']="Category information updated successfully !";
        $this->session->set_userdata($sdata);
        redirect('super_admin/manage_category');
    }
    public function calendar()
    {
        $data=array();
        $data['title']='Calendar';
        $data['admin_content']=$this->load->view('admin/calendar_view',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function logout()
    {
        $this->session->unset_userdata('admin_name');
        $this->session->unset_userdata('admin_id');
        $sdata=array();
        $sdata['message']='You are successfully logged out !';
        $this->session->set_userdata($sdata);
        redirect('admin_login','refresh');
    }
    public function add_product()
    {
        $data=array();
        $data['editor']=$this->data;
        $data['title']='Add Product';
        $data['all_published_manufacturer']=$this->super_admin_model->select_all_published_manufacturer();
        $data['all_published_category']=$this->super_admin_model->select_all_published_category();
        $data['admin_content']=$this->load->view('admin/add_product',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function save_product()
    {
        $data=array();
        $data['product_name']=$this->input->post('product_name',true);
        $data['product_model']=$this->input->post('product_model',true);
        $data['manufacturer_id']=$this->input->post('manufacturer_id',true);
        $data['product_price']=$this->input->post('product_price',true);
        $data['category_id']=$this->input->post('category_id',true);
        $data['product_short_description']=$this->input->post('product_short_description',true);
        $data['product_long_description']=$this->input->post('product_long_description',true);
        $data['product_quntity']=$this->input->post('product_quntity',true);
        $data['product_reorder_level']=$this->input->post('product_reorder_level',true);
        $data['publication_status']=$this->input->post('publication_status',true);
        $product_id = $this->super_admin_model->save_product_info($data);
        /*
         * Image upload star here..
         * */
        $data = array();
        $default_image = $this->input->post('default_image',true);
        $config['upload_path'] = 'images/product_image/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '5000';
        $config['max_width']  = '2024';
        $config['max_height']  = '1768';

        $this->load->library('upload',$config);

        if (!$this->upload->do_multi_upload('product_image_name'))
        {
            $error = $this->upload->display_errors();
            echo $error;
            exit();
        }
        else {
            $return = $this->upload->get_multi_upload_data();
            foreach ($return as $value) {
                $data['product_image_name'] = $config['upload_path'] . $value['file_name'];
                $data['product_id'] = $product_id;
                $data['default_image'] = $default_image;
                $this->super_admin_model->save_product_image_info($data);
                $default_image = 1;
            }
        }
        /*
         * FIle upload end here...
         * */

        if($this->input->post('product_special_price', true))
        {
            $data = array();
            $data['product_id'] = $product_id;
            $data['product_special_price'] = $this->input->post('product_special_price', true);
            $data['date_start'] = $this->input->post('date_start', true);
            $data['date_end'] = $this->input->post('date_end', true);
            $this->super_admin_model->save_product_special_price($data);
        }
        $sdata=array();
        $sdata['message']="Product added successfully !";
        $this->session->set_userdata($sdata);
        redirect('super_admin/add_product');
    }
    public function manage_product()
    {
        $data=array();
        $data['title']='Manage Product';
        $data['all_product']=$this->super_admin_model->select_all_product();
        $data['admin_content']=$this->load->view('admin/manage_product',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function unpublished_product($product_id)
    {
        $this->super_admin_model->unpublished_product_by_id($product_id);
        redirect('super_admin/manage_product');
    }
    public function published_product($product_id)
    {
        $this->super_admin_model->published_product_by_id($product_id);
        redirect('super_admin/manage_product');
    }
    public function delete_product($product_id)
    {
        $this->super_admin_model->delete_product_by_id($product_id);
        redirect('super_admin/manage_product');
    }
    public function manage_order()
    {
        $data=array();
        $this->load->library('pagination');

        $config['base_url'] = base_url() .'super_admin/manage_order/';
        $config['total_rows'] = $this->db->count_all('tbl_product');
        $config['per_page'] = 5;
        $config['full_tag_open'] = '<p>';
        $config['full_tag_close'] = '</p>';

        $this->pagination->initialize($config);
        $data['all_orders']=$this->super_admin_model->all_orders($config['per_page'], $this->uri->segment(3));
        $data['title']='Manage Order';
        $data['admin_content']=$this->load->view('admin/manage_orders',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function delete_order($order_id)
    {
        $this->super_admin_model->delete_order_by_id($order_id);
        redirect('super_admin/manage_orders');
    }
    public function details($order_id)
    {
        $data=array();
        $data['title']='Order Details';
        $data['order_info'] = $this->super_admin_model->select_order_by_id($order_id);
        $customer_id = $data['order_info']->customer_id;
        $shipping_id = $data['order_info']->shipping_id;
        $data['billing_info'] = $this->super_admin_model->select_customer_by_id($customer_id);
        $data['shipping_info'] = $this->super_admin_model->select_shipping_by_id($shipping_id);
        $data['order_details'] = $this->super_admin_model->order_details_info($order_id);
        $data['admin_content']=$this->load->view('admin/invoice',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function make_pdf($order_id)
    {
        $data=array();
        $data['order_info'] = $this->super_admin_model->select_order_by_id($order_id);
        $customer_id = $data['order_info']->customer_id;
        $shipping_id = $data['order_info']->shipping_id;
        $data['billing_info'] = $this->super_admin_model->select_customer_by_id($customer_id);
        $data['shipping_info'] = $this->super_admin_model->select_shipping_by_id($shipping_id);
        $data['order_details'] = $this->super_admin_model->order_details_info($order_id);


        $this->load->helper('dompdf');
        $html = $this->load->view('admin/invoice',$data,true);
        $filename=pdf_create($html, $data['order_info']->invoice_no);
        echo $filename;
    }
    public function order_search()
    {
        $search_text = $this->input->post('search_text');


        $data=array();
        $this->load->library('pagination');
        $config['base_url'] = base_url() .'super_admin/manage_order/';
        $config['total_rows'] = $this->db->count_all('tbl_product');
        $config['per_page'] = 5;
        $config['full_tag_open'] = '<p>';
        $config['full_tag_close'] = '</p>';

        $this->pagination->initialize($config);
        $data['all_orders']=$this->super_admin_model->order_search_info($config['per_page'], $this->uri->segment(3),$search_text);
        $data['title']='Manage Order';
        $data['admin_content']=$this->load->view('admin/manage_orders',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function stock_manage()
    {
        $data = array();
        $data['title']='Manage Stock';
        $data['all_product']=$this->super_admin_model->select_all_product();
        $data['admin_content']=$this->load->view('admin/stock_manage',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function update_product_quantity($product_id)
    {
        $data = array();
        $data['product_quntity'] = $this->input->post('product_quntity',true);
        $this->super_admin_model->update_product_quantity_by_id($product_id,$data);
        $sdata=array();
        $sdata['message']="Product Quantity updated successfully !";
        $this->session->set_userdata($sdata);
        redirect('super_admin/stock_manage');
    }

}