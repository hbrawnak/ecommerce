<div class="content_box">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="menu_box">
                    <h3 class="menu_head">Menu</h3>
                    <ul class="nav">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li><a href="<?= base_url(); ?>welcome/apparel">Apparel</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="<?= base_url(); ?>welcome/contact">Contact</a></li>
                    </ul>
                </div>
                <div class="side_banner">
                    <div class="banner_img"><img src="<?=base_url();?>assets/images/pic9.jpg" class="img-responsive" alt=""/></div>
                    <div class="banner_holder">
                        <h3>Now <br> is <br> Open!</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="dreamcrub">
                    <ul class="breadcrumbs">
                        <li class="home">
                            <a href="index.html" title="Go to Home Page">Home</a>&nbsp;
                            <span>&gt;</span>
                        </li>
                        <li class="women">
                            Contact Us
                        </li>
                    </ul>
                    <ul class="previous">
                        <li><a href="index.html">Back to Previous Page</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="singel_right">
                    <div class="lcontact span_1_of_contact">
                        <div class="contact-form">
                            <form method="post" action="contact-post.html">
                                <p class="comment-form-author"><label for="author">Your Name:</label>
                                    <input type="text" class="textbox" value="Enter your name here..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your name here...';}">
                                </p>
                                <p class="comment-form-author"><label for="author">Email:</label>
                                    <input type="text" class="textbox" value="Enter your email here..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
                                </p>
                                <p class="comment-form-author"><label for="author">Message:</label>
                                    <textarea value="Enter your message here..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Enter your message here...</textarea>
                                </p>
                                <input name="submit" type="submit" id="submit" value="Submit">
                            </form>
                        </div>
                    </div>
                    <div class="contact_grid span_2_of_contact_right">
                        <h3>Address</h3>
                        <div class="address">
                            <i class="pin_icon"></i>
                            <div class="contact_address">
                                Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="address">
                            <i class="phone"></i>
                            <div class="contact_address">
                                1-25-2568-897
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="address">
                            <i class="mail"></i>
                            <div class="contact_email">
                                info(at)surfhouse.com
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>