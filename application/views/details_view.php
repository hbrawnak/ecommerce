<div class="content_box">
<div class="container">
<div class="row">
<div class="col-md-3">
    <div class="menu_box">
        <h3 class="menu_head">Menu</h3>
        <ul class="nav">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li><a href="<?= base_url(); ?>welcome/apparel">Apparel</a></li>
            <li><a href="#">About</a></li>
            <li><a href="<?= base_url(); ?>welcome/contact">Contact</a></li>
        </ul>
    </div>
    <div class="category">
        <h3 class="menu_head">Category Options</h3>
        <?php
        foreach ($all_published_category as $v_category) {
            ?>
            <ul class="category_nav">
                <li><a href="<?php echo base_url('welcome/category');?>/<?php echo $v_category->category_id;?>"><?php echo $v_category->category_name; ?></a></li>

            </ul>
        <?php } ?>
    </div>
    <div class="category">
        <h3 class="menu_head">Brand Options</h3>
        <?php
        foreach ($all_published_manufacturer as $v_brand) {
            ?>
            <ul class="category_nav">
                <li><a href="<?php echo base_url('welcome/manufacturer');?>/<?php echo $v_brand->manufacturer_id;?>"><?php echo $v_brand->manufacturer_name; ?></a></li>
            </ul>
        <?php
        }
        ?>
    </div>
    <div class="tags">
        <h4 class="tag_head">Tags Widget</h4>
        <ul class="tags_links">
            <li><a href="#">Kitesurf</a></li>
            <li><a href="#">Super</a></li>
            <li><a href="#">Duper</a></li>
            <li><a href="#">Theme</a></li>
            <li><a href="#">Men</a></li>
            <li><a href="#">Women</a></li>
            <li><a href="#">Equipment</a></li>
            <li><a href="#">Best</a></li>
            <li><a href="#">Accessories</a></li>
            <li><a href="#">Men</a></li>
            <li><a href="#">Apparel</a></li>
            <li><a href="#">Super</a></li>
            <li><a href="#">Duper</a></li>
            <li><a href="#">Theme</a></li>
            <li><a href="#">Responsiv</a></li>
            <li><a href="#">Women</a></li>
            <li><a href="#">Equipment</a></li>
        </ul>
        <a href="#" class="link1">View all tags</a>
    </div>
    <div class="side_banner">
        <div class="banner_img"><img src="<?php echo base_url();?>assets/images/pic9.jpg" class="img-responsive" alt=""/></div>
        <div class="banner_holder">
            <h3>Now <br> is <br> Open!</h3>
        </div>
    </div>
</div>
<div class="col-md-9">
<div class="dreamcrub">
    <ul class="breadcrumbs">
        <li class="home">
            <a href="index.html" title="Go to Home Page">Home</a>&nbsp;
            <span>&gt;</span>
        </li>
        <li class="home">&nbsp;
            Apparel&nbsp;
            <span>&gt;</span>&nbsp;
        </li>
        <li class="home">&nbsp;
            &nbsp;Women
            <span>&gt;</span>&nbsp;
        </li>
        <li class="women">
            <?=$product_info->product_name;?>
        </li>
    </ul>
    <ul class="previous">
        <li><a href="index.html">Back to Previous Page</a></li>
    </ul>
    <div class="clearfix"></div>
</div>
<div class="singel_right">
    <div class="labout span_1_of_a1">
        <!-- start product_slider -->
        <ul id="etalage">
            <li>
                <a href="optionallink.html">
                    <img class="etalage_thumb_image" src="<?php echo base_url();?><?=$product_info->product_image_name?>" class="img-responsive" />

                </a>
            </li>

            <?php
            foreach($product_image as $image) {
                ?>
                <li>
                    <img class="etalage_source_image" src="<?php echo base_url(); ?><?=$image->product_image_name;?>"
                         class="img-responsive"/>
                </li>
            <?php
            }
            ?>


        </ul>
        <!-- end product_slider -->
    </div>

    <div class="cont1 span_2_of_a1">
        <h1><?=$product_info->product_name;?></h1>
        <h4><?=$product_info->manufacturer_name ?></h4>
        <ul class="rating">
            <li><a class="product-rate" href="#"> <label> </label></a> <span> </span></li>
            <li><a href="#">1 Review(s) Add Review</a></li>
            <div class="clearfix"></div>
        </ul>
        <div class="price_single">
            <?php
            $today = strtotime(date("Y-m-d"));
            $start_date = strtotime($product_info->date_start);
            $end_date = strtotime($product_info->date_end);
            if (($product_info->product_special_price) && (($today >= $start_date) && ($today <= $end_date))) {
                ?>
                <span class="reducedfrom">$<?=$product_info->product_price; ?></span>
                <span class="actual">$<?=$product_info->product_special_price; ?></span>
            <?php
            } else {
                ?>
                <span class="actual">$<?=$product_info->product_price; ?></span>
            <?php
            }
            ?>
            <!--<span class="reducedfrom">$140.00</span>
            <span class="actual">$120.00</span>-->
           <br/> <span class=""><?=$product_info->product_model;?></span>
        </div>
        <h2 class="quick">Quick Overview:</h2>

        <p class="quick_desc"><?=$product_info->product_short_description;?></p>
        <ul class="product-qty">
            <span>Size:</span>
            <select>
                <option>25</option>
                <option>26</option>
                <option>27</option>
                <option>28</option>
                <option>29</option>
                <option>30</option>
            </select>
        </ul>
        <ul class="product-qty">
            <span>Length:</span>
            <select>
                <option>31</option>
                <option>32</option>
                <option>33</option>
                <option>34</option>
                <option>35</option>
                <option>36</option>
            </select>
        </ul>
        <form method="post" action="<?=base_url();?>cart/add_to_cart/<?=$product_info->product_id;?>">
        <ul class="product-qty">
            <span>Quantity:</span>
            <select name="qty">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>
        </ul>
        <div class="btn_form">
                <input type="hidden" value="<?=$product_info->product_id;?>" name="product_id">
                <input type="submit" value="Add to Cart" title="">
        </div>
        </form>
    </div>


    <div class="clearfix"></div>
</div>
<div class="sap_tabs">
    <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
        <ul class="resp-tabs-list">
            <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Product Description</span></li>
            <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span>Reviews</span></li>
            <div class="clear"></div>
        </ul>
        <div class="resp-tabs-container">
            <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                <div class="facts">
                    <ul class="tab_list">
                        <li><a href="#"><?= $product_info->product_long_description ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
                <ul class="tab_list">
                    <li><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh
                            euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim
                            veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea
                            commodo consequat</a></li>
                    <li><a href="#">augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis
                            eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.
                            Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem.
                            Investigatione</a></li>
                    <li><a href="#">claritatem insitam; est usus legentis in iis qui facit eorum claritatem.
                            Investigationes demonstraverunt lectores leg</a></li>
                    <li><a href="#">Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit
                            litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi,
                            qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<h3 class="like">You Might Also Like</h3>
<ul id="flexiselDemo3">
    <li><img src="<?php echo base_url();?>assets/images/pic11.jpg" /><div class="grid-flex"><a href="#">Syenergy 2mm</a><p>Rs 850</p></div></li>
    <li><img src="<?php echo base_url();?>assets/images/pic10.jpg" /><div class="grid-flex"><a href="#">Surf Yoke</a><p>Rs 1050</p></div></li>
    <li><img src="<?php echo base_url();?>assets/images/pic9.jpg" /><div class="grid-flex"><a href="#">Salty Daiz</a><p>Rs 990</p></div></li>
    <li><img src="<?php echo base_url();?>assets/images/pic8.jpg" /><div class="grid-flex"><a href="#">Cheeky Zane</a><p>Rs 850</p></div></li>
    <li><img src="<?php echo base_url();?>assets/images/pic7.jpg" /><div class="grid-flex"><a href="#">Synergy</a><p>Rs 870</p></div></li>
</ul>
<script type="text/javascript">
    $(window).load(function() {
        $("#flexiselDemo3").flexisel({
            visibleItems: 3,
            animationSpeed: 1000,
            autoPlay: true,
            autoPlaySpeed: 3000,
            pauseOnHover: true,
            enableResponsiveBreakpoints: true,
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 1
                },
                landscape: {
                    changePoint:640,
                    visibleItems: 2
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 3
                }
            }
        });

    });
</script>
<script type="text/javascript" src="<?=base_url();?>assets/js/jquery.flexisel.js"></script>
<h3 class="recent">Recently Viewed</h3>
<ul id="flexiselDemo1">
    <li><img src="<?php echo base_url();?>assets/images/pic1.jpg" /><div class="grid-flex"><a href="#">Syenergy 2mm</a><p>Rs 850</p></div></li>
    <li><img src="<?php echo base_url();?>assets/images/pic2.jpg" /><div class="grid-flex"><a href="#">Surf Yoke</a><p>Rs 1050</p></div></li>
    <li><img src="<?php echo base_url();?>assets/images/pic3.jpg" /><div class="grid-flex"><a href="#">Salty Daiz</a><p>Rs 990</p></div></li>
    <li><img src="<?php echo base_url();?>assets/images/pic4.jpg" /><div class="grid-flex"><a href="#">Cheeky Zane</a><p>Rs 850</p></div></li>
    <li><img src="<?php echo base_url();?>assets/images/pic5.jpg" /><div class="grid-flex"><a href="#">Synergy</a><p>Rs 870</p></div></li>
</ul>
<script type="text/javascript">
    $(window).load(function() {
        $("#flexiselDemo1").flexisel({
            visibleItems: 3,
            animationSpeed: 1000,
            autoPlay: true,
            autoPlaySpeed: 3000,
            pauseOnHover: true,
            enableResponsiveBreakpoints: true,
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 1
                },
                landscape: {
                    changePoint:640,
                    visibleItems: 2
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 3
                }
            }
        });

    });
</script>
</div>
</div>
</div>
</div>