<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="fa fa-users">
            Manage Product
            <small>Product Data Tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Product</a></li>
            <li class="active">Manage Product</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage Product Data Table</h3>
                        <div class="form-group">
                            <?php
                            $message=$this->session->userdata('message');
                            if($message)
                            {
                                ?>
                                <div align="center" class="alert alert-success"><p>
                                        <?php echo $message; ?>
                                    </p>
                                </div>
                                <?php
                                $this->session->unset_userdata('message');
                            }
                            ?>
                        </div>

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Product Image</th>
                                <th>Name</th>
                                <th>Product Price</th>
                                <th>Product Quantity</th>
                                <th>Product Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($all_product as $v_product) {
                                ?>
                                <tr>
                                    <td><img height="50" width="50" src="<?php echo base_url() ?><?php echo $v_product->product_image_name; ?>"></td>
                                    <td><?php echo $v_product->product_name;?></td>
                                    <td>
                                        <?php
                                        $today = strtotime(date("Y-m-d"));
                                        $start_date = strtotime($v_product->date_start);
                                        $end_date = strtotime($v_product->date_end);
                                        if (($v_product->product_special_price) && (($today >= $start_date) && ($today <= $end_date))) {
                                            ?>
                                            <div>Normal: <?= $v_product->product_price; ?></div>
                                            <div>Special: <?= $v_product->product_special_price; ?></div>
                                        <?php
                                        } else {
                                            ?>
                                            <div>Normal: <?= $v_product->product_price; ?></div>
                                        <?php
                                        }
                                        ?>



                                    </td>
                                    <td><?php echo $v_product->product_quntity;?></td>
                                    <td>
                                        <?php
                                        if($v_product->publication_status == 0)
                                        {
                                            echo 'Published';
                                        }
                                        else
                                        {
                                            echo 'Unpublished';
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <a href="#" type="button" class="btn btn-info" title="Edit">Edit</a>
                                        <a href="<?php echo base_url();?>super_admin/delete_product/<?php echo $v_product->product_id?>" onclick="return checkDelete();" type="button" class="btn btn-danger" title="Delete">Delete</a>

                                        <?php
                                        if($v_product->publication_status == 0)
                                        {
                                            ?>
                                            <a href="<?php echo base_url();?>super_admin/unpublished_product/<?php echo $v_product->product_id?>" type="button" class="btn bg-olive margin" title="Unpublished">Unpublished</a>
                                        <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <a href="<?php echo base_url();?>super_admin/published_product/<?php echo $v_product->product_id?>" type="button" class="btn bg-purple margin" title="Published">Published</a>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->