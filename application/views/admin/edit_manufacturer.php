<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="fa fa-edit">
            Update Manufacturer
            <small>Company Name</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Manufacturer</a></li>
            <li><a href="#">Add Manufacturer</a></li>
            <li class="active">Edit Manufacturer</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="form-group">
                        <?php
                        $message=$this->session->userdata('message');
                        if($message)
                        {
                            ?>
                            <div class="alert alert-success"><p>
                                    <?php echo $message; ?>
                                </p>
                            </div>
                            <?php
                            $this->session->unset_userdata('message');
                        }
                        ?>
                    </div>

                    <div class="box-header with-border">
                        <h3 class="box-title">Please fill up the form...</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="<?php echo base_url();?>super_admin/update_manufacturer/<?php echo $manufacturer_info->manufacturer_id?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Manufacturer Name</label>
                                <input type="text" class="form-control" name="manufacturer_name" value="<?php echo $manufacturer_info->manufacturer_name;?>" id="exampleInputEmail1" placeholder="Manufacturer Name" required="">
                            </div>
                            <div class="form-group">
                                <label>Manufacturer Description</label>
                                <textarea class="form-control" name="manufacturer_description" rows="3" placeholder="Enter a description ..." required=""><?php echo $manufacturer_info->manufacturer_description;?></textarea>
                            </div>
                            <div class="form-group">
                                <div><label>Publication Status</label></div>
                                <label>
                                    <?php
                                    if($manufacturer_info->publication_status==0)
                                    {
                                        ?>
                                        <input type="radio" name="publication_status" id="button1" value="0"
                                               checked> <small>Published</small>
                                    <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <input type="radio" name="publication_status" id="button1" value="0"><small>Published</small>
                                    <?php
                                    }
                                    ?>
                                </label>
                                <br/>
                                <label>

                                    <?php
                                    if($manufacturer_info->publication_status==1)
                                    {
                                        ?>
                                        <input type="radio" name="publication_status" id="button2"
                                               value="1" checked><small>Unpublished</small>
                                    <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <input type="radio" name="publication_status" id="button2"
                                               value="1"><small>Unpublished</small>
                                    <?php } ?>
                                </label>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Update Manufacturer</button>
                            <button type="reset" class="btn btn-danger">Reset</button>

                    </form>

                    <form method="post" action="<?php echo base_url();?>super_admin/manage_manufacturer">
                        <button align="center" style="margin-left: 234px;margin-top: -56px;" type="submit" class="btn btn-default">Cancel</button>
                    </form>

                    </div>
                </div><!-- /.box -->
            </div><!--/.col (left) -->
            <!-- right column -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->