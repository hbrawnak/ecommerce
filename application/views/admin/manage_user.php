<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="fa fa-users">
            Manage User
            <small>User Data Tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User</a></li>
            <li class="active">Manage User</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage User Data Table</h3>
                        <form action="<?php echo base_url();?>super_admin/user_search" method="post" class="sidebar-form">
                            <div class="input-group">
                                <input type="text" name="search_text" class="form-control" placeholder="Search..."/>
                                  <span class="input-group-btn">
                                    <button type="submit" name="search" value="Search order" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                    </button>
                                  </span>
                            </div>
                        </form>

                        <div class="form-group">
                            <?php
                            $message = $this->session->userdata('message');
                            if ($message) {
                                ?>
                                <div align="center" class="alert alert-success"><p>
                                        <?php echo $message; ?>
                                    </p>
                                </div>
                                <?php
                                $this->session->unset_userdata('message');
                            }
                            ?>
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>User Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>User Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($all_user as $v_user) {
                                ?>
                                <tr>
                                    <td><?php echo $v_user->user_id; ?></td>
                                    <td><?php echo $v_user->user_first_name . ' ' . $v_user->user_last_name; ?></td>
                                    <td><?php echo $v_user->user_email; ?></td>
                                    <td>
                                        <?php
                                        if ($v_user->user_status == 0) {
                                            echo 'Active';
                                        } else {
                                            echo 'Inactive';
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>super_admin/edit_user/<?php echo $v_user->user_id ?>"
                                           type="button" class="btn btn-info" title="Edit">Edit</a>
                                        <a href="<?php echo base_url(); ?>super_admin/delete_user/<?php echo $v_user->user_id ?>"
                                           onclick="return checkDelete();" type="button" class="btn btn-danger"
                                           title="Delete">Delete</a>

                                        <?php
                                        if ($v_user->user_status == 0) {
                                            ?>
                                            <a href="<?php echo base_url(); ?>super_admin/inactive_user/<?php echo $v_user->user_id ?>"
                                               type="button" class="btn bg-olive margin" title="Inactive">Inactive</a>
                                        <?php
                                        } else {
                                            ?>
                                            <a href="<?php echo base_url(); ?>super_admin/active_user/<?php echo $v_user->user_id ?>"
                                               type="button" class="btn bg-purple margin" title="Active">Active</a>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                            <tfooter>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfooter>
                        </table>

                    </div>
                    <!-- /.box-body -->
                    <div align="center">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->