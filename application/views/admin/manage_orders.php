<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="fa fa-users">
            Manage Orders
            <small>Orders Data Tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Orders</a></li>
            <li class="active">Manage Orders</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage Orders Data Table</h3>
                        <div class="form-group">
                        <form action="<?php echo base_url();?>super_admin/order_search" method="post" class="sidebar-form">
                            <div class="input-group">
                                <input type="text" name="search_text" class="form-control" placeholder="Search..."/>
                                  <span class="input-group-btn">
                                    <button type="submit" name="search" value="Search order" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                    </button>
                                  </span>
                            </div>
                        </form>
                        </div>
                        <div class="form-group">
                            <?php
                            $message = $this->session->userdata('message');
                            if ($message) {
                                ?>
                                <div align="center" class="alert alert-success"><p>
                                        <?php echo $message; ?>
                                    </p>
                                </div>
                                <?php
                                $this->session->unset_userdata('message');
                            }
                            ?>
                        </div>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Order Id</th>
                                <th>Invoice No</th>
                                <th>Customer Name</th>
                                <th>Order Total</th>
                                <th>Order Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($all_orders as $v_orders) {
                                ?>
                                <tr>
                                    <td><?php echo $v_orders->order_id; ?></td>
                                    <td><?php echo $v_orders->invoice_no; ?></td>
                                    <td><?php echo $v_orders->user_first_name . ' ' . $v_orders->user_last_name; ?></td>
                                    <td><?php echo $v_orders->order_total; ?></td>
                                    <td>
                                        <?php
                                        if ($v_orders->order_status == 0) {
                                            echo 'Pending';
                                        } elseif ($v_orders->order_status == 1) {
                                            echo 'Confirm';
                                        } else {
                                            echo 'Cancel';
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <a href="#" type="button"
                                           class="fa fa-fw fa-edit" title="Edit"></a>
                                        <a href="<?php echo base_url(); ?>super_admin/delete_order/<?php echo $v_orders->order_id; ?>"
                                           onclick="return checkDelete();" type="button" class="fa fa-fw fa-close"
                                           title="Delete"></a>
                                        <a href="<?php echo base_url(); ?>super_admin/details/<?php echo $v_orders->order_id; ?>"
                                           type="button" class="fa fa-fw fa-newspaper-o" title="Details"></a>
                                        <a href="<?php echo base_url(); ?>super_admin/make_pdf/<?php echo $v_orders->order_id; ?>"
                                           type="button" class="fa fa-fw fa-file-pdf-o" title="Create PDF"></a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                        <div align="center"><?php echo $this->pagination->create_links(); ?></div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div><!-- /.content-wrapper -->