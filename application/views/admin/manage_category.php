<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="fa fa-users">
            Manage Category
            <small>Category Data Tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Product</a></li>
            <li class="active">Manage Category</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage Category Data Table</h3>
                        <div class="form-group">
                            <?php
                            $message=$this->session->userdata('message');
                            if($message)
                            {
                                ?>
                                <div align="center" class="alert alert-success"><p>
                                        <?php echo $message; ?>
                                    </p>
                                </div>
                                <?php
                                $this->session->unset_userdata('message');
                            }
                            ?>
                        </div>

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Category Id</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Category Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($all_category as $v_category) {
                                ?>
                                <tr>
                                    <td><?php echo $v_category->category_id;?></td>
                                    <td><?php echo $v_category->category_name;?></td>
                                    <td><?php echo $v_category->category_description;?></td>
                                    <td>
                                        <?php
                                        if($v_category->publication_status == 0)
                                        {
                                            echo 'Published';
                                        }
                                        else
                                        {
                                            echo 'Unpublished';
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url();?>super_admin/edit_category/<?php echo $v_category->category_id?>" type="button" class="btn btn-info" title="Edit">Edit</a>
                                        <a href="<?php echo base_url();?>super_admin/delete_category/<?php echo $v_category->category_id?>" onclick="return checkDelete();" type="button" class="btn btn-danger" title="Delete">Delete</a>

                                        <?php
                                        if($v_category->publication_status == 0)
                                        {
                                            ?>
                                            <a href="<?php echo base_url();?>super_admin/unpublished_category/<?php echo $v_category->category_id?>" type="button" class="btn bg-olive margin" title="Unpublished">Unpublished</a>
                                        <?php
                                        }
                                        else
                                        {
                                            ?>
                                            <a href="<?php echo base_url();?>super_admin/published_category/<?php echo $v_category->category_id?>" type="button" class="btn bg-purple margin" title="Published">Published</a>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->