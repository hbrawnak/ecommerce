<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="fa fa-users">
        Add User
        <small>Normal User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User</a></li>
        <li class="active">Add User</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="row">
<!-- left column -->
<div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="form-group">
            <?php
            $message=$this->session->userdata('message');
            if($message)
            {
                ?>
                <div class="alert alert-success"><p>
                    <?php echo $message; ?>
                </p>
                </div>
                <?php
                $this->session->unset_userdata('message');
            }
            ?>
        </div>

        <div class="box-header with-border">
            <h3 class="box-title">Please fill up the form...</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="<?php echo base_url();?>super_admin/save_user">
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="text" class="form-control" name="user_first_name" id="exampleInputEmail1" placeholder="First Name" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Last Name</label>
                    <input type="text" class="form-control" name="user_last_name" id="exampleInputEmail1" placeholder="Last Name" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">User Name</label>
                    <input type="text" class="form-control" name="user_name" id="exampleInputEmail1" placeholder="User Name" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" name="user_email" onblur="makerequest(this.value,'res');" placeholder="Email Address" required=""><span class="res" id="res"></span>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" name="user_password" id="exampleInputPassword1" placeholder="Password" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Confirm Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Mobile No</label>
                    <input type="text" class="form-control" name="mobile_no" id="exampleInputEmail1" placeholder="Mobile No" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Address</label>
                    <textarea  class="form-control" name="address" id="exampleInputEmail1" placeholder="Address" required=""></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Zip Code</label>
                    <input type="text" class="form-control" name="zip_code" id="exampleInputEmail1" placeholder="Zip Code" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">City</label>
                    <input type="text" class="form-control" name="city" id="exampleInputEmail1" placeholder="City" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Country</label>
                    <input type="text" class="form-control" name="country" id="exampleInputEmail1" placeholder="Country" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">State</label>
                    <input type="text" class="form-control" name="state" id="exampleInputEmail1" placeholder="State" required="">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">News Letter</label><br/>
                    <input type="radio" value="1" name="newsletter" id="exampleInputEmail1" checked> Yes <br/>
                    <input type="radio" value="2" name="newsletter" id="exampleInputEmail1"> No
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-success">Add User</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
        </form>
    </div><!-- /.box -->
</div><!--/.col (left) -->
<!-- right column -->
</div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->