<?php
$contents = $this->cart->contents();
$count = count($contents);
//echo $count;
/*echo '<pre>';
print_r($contents);
exit();*/

?>

<div class="main">
    <div class="container">
        <?php
        if ($count == 0) {
            ?>
            <div class="register">
                <h4 class="title">Shopping cart is empty</h4>

                <p class="cart">You have no items in your shopping cart.<br>Click<a href="<?php echo base_url(); ?>">
                        here</a> to
                    continue shopping</p>
            </div>
        <?php
        } else {
            ?>
            <div class="register">
                <h4 class="title">Your Shopping cart</h4>

                <div align="right" class="register-top-grid">
                    <a href="<?php echo base_url(); ?>">Continue Shopping...>></a>
                </div>

                <table border="1" style="width: 100%" class="cart">
                    <thead>
                    <tr style="background-color: #808080">
                        <th>Image</th>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Sub Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($contents as $values) {
                        ?>
                        <tr>
                            <td><img height="50" width="50" src="<?php echo base_url() . $values['image']; ?>"></td>
                            <td><?php echo $values['name']; ?></td>
                            <td>
                                <form method="post" action="<?= base_url(); ?>cart/update_cart/">
                                    <div style="float: left">
                                        <input name="qty" type="text" value="<?php echo $values['qty']; ?>" size="5"
                                               placeholder="Write Quantity">
                                        <input name="rowid" type="hidden" value="<?php echo $values['rowid']; ?>"
                                               size="5">
                                    </div>
                                    &nbsp;&nbsp;&nbsp;
                                    <a style="float: right"
                                       href="<?= base_url(); ?>cart/update_cart/<?php echo $values['rowid']; ?>">Delete</a>
                                    &nbsp;&nbsp;&nbsp;
                                    <button style="float: left" type="submit">Reload</button>
                                </form>
                            </td>
                            <td><?php echo $values['price']; ?></td>
                            <td><?php echo $values['subtotal']; ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Grand Total</th>
                        <th><?php echo $this->cart->total(); ?></th>
                    </tr>
                    </tfoot>
                </table>
                <br>
                <?php
                $user_id = $this->session->userdata('user_id');
                $shipping_id = $this->session->userdata('shipping_id');
                if ($user_id == NULL) {
                    ?>
                    <div align="right"><a href="<?php echo base_url('checkout') . '?ref=' . current_uri(); ?>">Checkout
                            >></a></div>
                <?php
                }
                if ($user_id != NULL && $shipping_id == NULL) {
                    $this->load->view('shipping_view');
                }
                if ($user_id != NULL && $shipping_id != NULL) {
                    $this->load->view('payment_form');
                }
                ?>
            </div>
        <?php
        }
        ?>
    </div>
</div>