<div class="content_box">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="menu_box">
                    <h3 class="menu_head">Menu</h3>
                    <ul class="nav">
                        <li><a href="<?=base_url();?>">Home</a></li>
                        <li><a href="<?=base_url();?>welcome/about">About</a></li>
                        <li><a href="<?=base_url();?>welcome/apparel">Apparel</a></li>
                        <li><a href="<?=base_url();?>welcome/apparel">Surf Apparel</a></li>
                        <li><a href="<?=base_url();?>welcome/apparel">Windsurf</a></li>
                        <li><a href="<?=base_url();?>welcome/apparel">Kitesurf</a></li>
                        <li><a href="apparel.html">Accessories</a></li>
                        <li><a href="apparel.html">Sale</a></li>
                        <li><a href="apparel.html">Brands</a></li>
                        <li><a href="apparel.html">Blog</a></li>
                        <li><a href="apparel.html">Gadgets</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="dreamcrub">
                    <ul class="breadcrumbs">
                        <li class="home">
                            <a href="<?=base_url();?>" title="Go to Home Page">Home</a>&nbsp;
                            <span>&gt;</span>
                        </li>
                        <li class="home">&nbsp;
                            &nbsp;Account
                            <span>&gt;</span>&nbsp;
                        </li>
                        <li class="women">
                            Login
                        </li>
                    </ul>
                    <ul class="previous">
                        <li><a href="<?=base_url();?>">Back to Previous Page</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="account_grid">
                    <div class="col-md-6 login-left">
                        <h3>NEW CUSTOMERS</h3>
                        <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                        <a class="acount-btn" href="<?=base_url();?>checkout/register">Create an Account</a>
                    </div>
                    <div class="col-md-6 login-right">
                        <h3>REGISTERED CUSTOMERS</h3>
                        <?php
                        $message=$this->session->userdata('message');
                        if($message)
                        {
                            ?>
                            <div class="success_msg"><bold>
                                <?php echo $message; ?>
                            </div></bold>
                            <?php
                            $this->session->unset_userdata('message');
                        }
                        else
                        {
                        ?>
                        <p>If you have an account with us, please log in.</p>
                        <?php } ?>
                        <form method="post" action="<?=base_url();?>checkout/check_login">
                            <div>
                                <span>Email Address<label>*</label></span>
                                <input type="email" name="user_email" placeholder="Email">
                            </div>
                            <div>
                                <input type="hidden" name="user_status" value="0">
                            </div>
                            <div>
                                <span>Password<label>*</label></span>
                                <input type="password" name="user_password" placeholder="Password">
                            </div>
                            <a class="forgot" href="#">Forgot Your Password?</a>
                            <input type="submit" value="Login">
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
    </div>
</div>
