<div class="main">
    <br>
    <div class="container">
        <div class="account_grid">
            <div class="col-md-6 login-right">
                <h3>REGISTERED CUSTOMERS</h3>
                <?php
                $message = $this->session->userdata('message');
                if ($message) {
                    ?>
                    <div class="success_msg">
                        <bold>
                            <?php echo $message; ?>
                    </div></bold>
                    <?php
                    $this->session->unset_userdata('message');
                } else {
                    ?>
                    <p>If you have an account with us, please log in.</p>
                <?php } ?>
                <form method="post" action="<?= base_url('checkout/check_login'). "?ref=" .$this->input->get('ref') ; ?>">
                    <div>
                        <span>Email Address<label>*</label></span>
                        <input type="email" name="user_email" placeholder="Email">
                    </div>
                    <div>
                        <input type="hidden" name="user_status" value="0">
                    </div>
                    <div>
                        <span>Password<label>*</label></span>
                        <input type="password" name="user_password" placeholder="Password">
                    </div>
                    <a class="forgot" href="#">Forgot Your Password?</a>
                    <input type="submit" value="Login">
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="container">
    <div class="register">
        <form method="post" action="<?php echo base_url(); ?>checkout/save_user">
            <div class="register-top-grid">
                <h3>PERSONAL INFORMATION FOR SIGN UP</h3>

                <div>
                    <span>First Name<label>*</label></span>
                    <input type="text" name="user_first_name" placeholder="First Name" required="">
                </div>
                <div>
                    <span>Last Name<label>*</label></span>
                    <input type="text" name="user_last_name" placeholder="Last Name" required="">
                </div>
                <div>
                    <span>User Name<label>*</label></span>
                    <input type="text" name="user_name" placeholder="User Name" required="">
                </div>
                <div>
                    <span>Email Address<label>*</label></span>
                    <input type="email" name="user_email" onblur="makerequest(this.value,'res');" placeholder="Email Address" required=""><span class="res" id="res"></span>
                </div>
                <div>
                    <span>Mobile No<label>*</label></span>
                    <input type="text" name="mobile_no" placeholder="Mobile No" required="">
                </div>
                <div>
                    <span>Address<label>*</label></span>
                    <textarea placeholder="Address" cols="62" name="address"></textarea>
                </div>
                <div>
                    <span>Zip Code<label>*</label></span>
                    <input type="text" name="zip_code" placeholder="Zip Code" required="">
                </div>
                <div>
                    <span>City<label>*</label></span>
                    <input type="text" name="city" placeholder="City" required="">
                </div>
                <div>
                    <span>Country<label>*</label></span>
                    <input type="text" name="country" placeholder="Country" required="">
                </div>
                <div>
                    <span>State<label>*</label></span>
                    <input type="text" name="state" placeholder="State" required="">
                </div>
                <div>
                    <span>Newsletter<label>*</label></span>
                    <input type="radio" name="newsletter" value="1"> Yes<br>
                    <input type="radio" name="newsletter" value="2"> No
                </div>
            </div>
            <div class="register-bottom-grid">
                <div>
                    <span>Password<label>*</label></span>
                    <input type="password" name="user_password" placeholder="Password" required="">
                </div>
                <div>
                    <span>Confirm Password<label>*</label></span>
                    <input type="password" placeholder="Confirm Password" required="">
                </div>
            </div>

            <div class="register-but">
                <div class="clearfix"></div>
                <input id="register_btn" type="submit" value="submit">
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>