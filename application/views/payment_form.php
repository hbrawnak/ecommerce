<div class="container">
    <div class="register">
        <form method="post" action="<?php echo base_url();?>checkout/confirm_order">
            <div class="register-top-grid">
                <h3>Payment Method</h3>
                <div>
                    <input type="radio" name="payment_type" value="cash_on_delivery" checked> Cash On Delivery<br>
                    <input type="radio" name="payment_type" value="paypal"> Paypal
                </div>
            </div>
            <div class="register-but">
                <div class="clearfix"></div>
                <input id="register_btn" type="submit" value="continue"><br/><br/>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>