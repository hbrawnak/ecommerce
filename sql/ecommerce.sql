-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2015 at 05:59 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
`admin_id` int(2) NOT NULL,
  `admin_name` varchar(50) NOT NULL,
  `admin_email_address` varchar(100) NOT NULL,
  `admin_password` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email_address`, `admin_password`) VALUES
(1, 'Md. Habibur Rahman', 'hbrawnak@gmail.com', 'e10adc3949ba59abbe56e057f20f883e');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
`category_id` int(5) NOT NULL,
  `category_name` varchar(80) NOT NULL,
  `category_description` text NOT NULL,
  `publication_status` tinyint(4) NOT NULL COMMENT 'publication_status=0 category published and publication_status=1 category unpublished',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_name`, `category_description`, `publication_status`, `created_date_time`) VALUES
(1, 'Electronics', 'All types of electronic product', 0, '2015-08-16 02:41:53'),
(2, 'Mobile', 'All types of mobile phone', 0, '2015-08-16 03:03:52'),
(3, 'Attires', 'It''s attires', 0, '2015-08-18 07:49:14'),
(4, 'Fashion Accessories ', 'Here all kind of fashion accessories you will get.', 0, '2015-08-19 03:54:50'),
(5, 'Office Furniture', 'Everything you will get for office decoration.', 0, '2015-08-19 03:55:46'),
(6, 'Computer and Gaming', 'All kinds of software, game and computer accessories you will get here. ', 1, '2015-08-19 03:57:24'),
(7, 'Sharee', 'Sharee', 0, '2015-08-28 09:12:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE IF NOT EXISTS `tbl_order` (
`order_id` int(7) NOT NULL,
  `customer_id` int(5) NOT NULL,
  `shipping_id` int(7) NOT NULL,
  `payment_id` int(5) NOT NULL,
  `invoice_no` varchar(15) NOT NULL,
  `order_status` tinyint(1) NOT NULL COMMENT 'order_status=0 pending, order_status=1 confirm, order_status=2 cancel',
  `order_total` float NOT NULL,
  `order_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `due_date` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `customer_id`, `shipping_id`, `payment_id`, `invoice_no`, `order_status`, `order_total`, `order_date_time`, `due_date`) VALUES
(1, 0, 4, 4, '', 0, 1000, '2015-08-25 19:38:17', ''),
(2, 0, 4, 5, '', 0, 1000, '2015-08-25 19:39:24', ''),
(3, 0, 4, 6, '', 0, 1290, '2015-08-25 19:44:39', ''),
(4, 0, 4, 7, '', 0, 1290, '2015-08-25 19:45:54', ''),
(5, 14, 4, 8, '', 0, 1290, '2015-08-25 19:47:12', ''),
(6, 17, 5, 9, '', 0, 796, '2015-08-26 02:05:43', ''),
(7, 14, 6, 10, '', 0, 1318, '2015-08-26 06:27:47', ''),
(8, 14, 6, 11, '', 0, 1318, '2015-08-26 06:28:52', ''),
(9, 14, 7, 12, 'inv-2015-9', 0, 870, '2015-08-26 14:45:10', ''),
(10, 15, 8, 13, 'inv-2015-10', 0, 750, '2015-08-27 04:45:53', '2015-30-08'),
(11, 1, 9, 14, 'inv-2015-11', 0, 690, '2015-08-28 06:35:51', '2015-31-08'),
(12, 1, 9, 15, 'inv-2015-12', 0, 690, '2015-08-28 06:39:44', '2015-31-08'),
(13, 1, 9, 16, 'inv-2015-13', 0, 460, '2015-08-28 06:41:26', '2015-31-08'),
(14, 18, 10, 17, 'inv-2015-14', 0, 920, '2015-08-28 08:57:22', '2015-31-08'),
(15, 20, 11, 18, 'inv-2015-15', 0, 2700, '2015-08-29 03:03:24', '2015-01-09'),
(16, 20, 12, 19, 'inv-2015-16', 0, 690, '2015-08-30 02:14:13', '2015-02-09'),
(17, 18, 13, 20, 'inv-2015-17', 0, 250, '2015-09-04 14:04:41', '2015-07-09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_details`
--

CREATE TABLE IF NOT EXISTS `tbl_order_details` (
`order_details_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_price` float NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_sales_quantity` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_order_details`
--

INSERT INTO `tbl_order_details` (`order_details_id`, `order_id`, `product_id`, `product_price`, `product_name`, `product_sales_quantity`) VALUES
(1, 2, 4, 250, 'Peeky Cropped', 2),
(2, 2, 1, 250, 'Syenergy 2mm', 2),
(3, 3, 4, 250, 'Peeky Cropped', 2),
(4, 3, 1, 250, 'Syenergy 2mm', 2),
(5, 3, 2, 290, 'Surf Yoke', 1),
(6, 4, 4, 250, 'Peeky Cropped', 2),
(7, 4, 1, 250, 'Syenergy 2mm', 2),
(8, 4, 2, 290, 'Surf Yoke', 1),
(13, 7, 3, 230, 'Salty Daiz', 4),
(14, 7, 5, 199, 'Cheeky Zane', 2),
(15, 8, 3, 230, 'Salty Daiz', 4),
(16, 8, 5, 199, 'Cheeky Zane', 2),
(17, 9, 2, 290, 'Surf Yoke', 3),
(18, 10, 4, 250, 'Peeky Cropped', 3),
(19, 11, 3, 230, 'Salty Daiz', 3),
(20, 12, 3, 230, 'Salty Daiz', 3),
(21, 14, 3, 230, 'Salty Daiz', 4),
(22, 15, 10, 900, 'Designed Party wear', 3),
(23, 16, 3, 230, 'Salty Daiz', 3),
(24, 17, 1, 250, 'Syenergy 2mm', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE IF NOT EXISTS `tbl_payment` (
`payment_id` int(7) NOT NULL,
  `payment_type` varchar(20) NOT NULL,
  `payment_status` tinyint(1) NOT NULL COMMENT '0=pending, 1=confirm, 2=cancel',
  `payment_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`payment_id`, `payment_type`, `payment_status`, `payment_date_time`) VALUES
(1, 'cash_on_delivery', 0, '2015-08-25 18:30:43'),
(2, 'cash_on_delivery', 0, '2015-08-25 19:32:26'),
(3, 'cash_on_delivery', 0, '2015-08-25 19:35:22'),
(4, 'cash_on_delivery', 0, '2015-08-25 19:38:17'),
(5, 'cash_on_delivery', 0, '2015-08-25 19:39:24'),
(6, 'cash_on_delivery', 0, '2015-08-25 19:44:39'),
(7, 'cash_on_delivery', 0, '2015-08-25 19:45:54'),
(8, 'cash_on_delivery', 0, '2015-08-25 19:47:12'),
(9, 'cash_on_delivery', 0, '2015-08-26 02:05:43'),
(10, 'cash_on_delivery', 0, '2015-08-26 06:27:47'),
(11, 'cash_on_delivery', 0, '2015-08-26 06:28:52'),
(12, 'cash_on_delivery', 0, '2015-08-26 14:45:10'),
(13, 'cash_on_delivery', 0, '2015-08-27 04:45:53'),
(14, 'cash_on_delivery', 0, '2015-08-28 06:35:51'),
(15, 'cash_on_delivery', 0, '2015-08-28 06:39:44'),
(16, 'cash_on_delivery', 0, '2015-08-28 06:41:26'),
(17, 'cash_on_delivery', 0, '2015-08-28 08:57:22'),
(18, 'cash_on_delivery', 0, '2015-08-29 03:03:24'),
(19, 'cash_on_delivery', 0, '2015-08-30 02:14:13'),
(20, 'cash_on_delivery', 0, '2015-09-04 14:04:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
`product_id` int(5) NOT NULL,
  `category_id` int(5) NOT NULL,
  `manufacturer_id` int(5) NOT NULL,
  `product_name` varchar(80) NOT NULL,
  `product_model` varchar(20) NOT NULL,
  `product_price` int(10) NOT NULL,
  `product_quntity` int(10) NOT NULL,
  `product_reorder_level` int(3) NOT NULL,
  `product_short_description` text NOT NULL,
  `product_long_description` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL COMMENT 'if publication status==0 product published and if publication status==1 product unpublished',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `category_id`, `manufacturer_id`, `product_name`, `product_model`, `product_price`, `product_quntity`, `product_reorder_level`, `product_short_description`, `product_long_description`, `publication_status`, `created_date_time`) VALUES
(1, 3, 1, 'Syenergy 2mm', '95321456', 280, 19, 10, 'Syenergy 2mm is an exclusive designed wear for night and hangout. ', '<p>Syenergy 2mm is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>\r\n\r\n<p>Syenergy 2mm is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>\r\n', 0, '2015-08-19 05:44:20'),
(2, 4, 3, 'Surf Yoke', '32654125', 290, 50, 10, 'Surf Yoke is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh. ', '<p>Surf Yoke is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n', 0, '2015-08-19 05:54:00'),
(3, 3, 4, 'Salty Daiz', '98754525', 270, 47, 10, 'Salty Daiz is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh.', '<p>Salty Daiz is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n', 0, '2015-08-19 05:59:20'),
(4, 4, 5, 'Peeky Cropped', '65945132', 250, 40, 10, 'Peeky Cropped is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh.', '<pre>\r\nPeeky Cropped is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</pre>\r\n', 0, '2015-08-19 06:19:31'),
(5, 4, 2, 'Cheeky Zane', '54698521', 240, 50, 10, 'Cheeky Zane is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh. ', '<pre>\r\nCheeky Zane is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</pre>\r\n\r\n<p> </p>\r\n', 0, '2015-08-19 06:24:55'),
(6, 4, 1, 'Kamij', '54653215', 310, 15, 10, 'Kurti is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh.', '<pre>\r\nKurti is an exclusive designed wear. It’s very comfortable and easy to wear. 100% cotton made in Bangladesh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</pre>\r\n\r\n<p> </p>\r\n', 0, '2015-08-19 07:57:35'),
(7, 7, 6, 'Sharee', 's-201574', 950, 50, 10, 'Beautiful Reddish Color Muslin Sharee from a re-known Fashion House. Beautifully crafted by Embroidery, Karchupi & Aplik work all over the Sharee. Matching blouse will be included with the Sharee.', '<p>Beautiful Reddish Color Muslin Sharee from a re-known Fashion House. Beautifully crafted by Embroidery, Karchupi & Aplik work all over the Sharee. Matching blouse will be included with the Sharee.</p>\r\n\r\n<p>Beautiful Reddish Color Muslin Sharee from a re-known Fashion House. Beautifully crafted by Embroidery, Karchupi & Aplik work all over the Sharee. Matching blouse will be included with the Sharee.</p>\r\n', 0, '2015-08-28 09:17:25'),
(8, 7, 6, 'Zarif Sharee', 'z-215465', 950, 50, 10, 'Dhakaiaa Jamdani store is now so popular and biggest online page for jamdani sharee Dhakaiaa Jamdani, Tangail Saree Kutir along with other sari stores on Baily', '<p>Dhakaiaa Jamdani store is now so popular and biggest online page for jamdani shareeDhakaiaa Jamdani, Tangail Saree Kutir along with other sari stores on Baily. Dhakaiaa Jamdani store is now so popular and biggest online page for jamdani sharee. Dhakaiaa Jamdani, Tangail Saree Kutir along with other sari stores on BailyDhakaiaa Jamdani store is now so popular and biggest online page for jamdani sharee.Dhakaiaa Jamdani, Tangail Saree Kutir along with other sari stores on Baily. Dhakaiaa Jamdani store is now so popular and biggest online page for jamdani shareeDhakaiaa Jamdani, Tangail Saree Kutir along with other sari stores on Baily</p>\r\n', 1, '2015-08-28 11:09:32'),
(9, 4, 2, 'Designed Party wear', 'p-4656', 1000, 50, 10, 'ExploreTex Sourcing Company Establish as a Garments Manufacturer, Buying or Sourcing Agent, Importer and Exporter to Sourcing Buyer from Abroad and Make a Good Deal with them to Fulfill Their Requirements.', '<p>As per Buyer Requirement with Any Design and Any Style with any kind of washing.</p>\r\n\r\n<p>ExploreTex Sourcing Company Establish as a Garments Manufacturer, Buying or Sourcing Agent, Importer and Exporter to Sourcing Buyer from Abroad and Make a Good Deal with them to Fulfill Their Requirements. Our mission is to produce the best quality products and make long term business with our valued buyers. ExploreTex Sourcing Company doing work with reputed garments industry. We work with non Compliance, Compliance, BSCI, OEKO TEX, WRAP, CTPAT, WalMart 350 License Holder, etc Certified Factory. There are large numbers of expert Management and worker. They work in a state of the art factory environment. As a supplier, commitment and excellence is our topmost priority.</p>\r\n', 0, '2015-08-28 11:15:43'),
(10, 4, 2, 'Designed Party wear', 'p-4656', 1000, 47, 10, 'ExploreTex Sourcing Company Establish as a Garments Manufacturer, Buying or Sourcing Agent, Importer and Exporter to Sourcing Buyer from Abroad and Make a Good Deal with them to Fulfill Their Requirements.', '<p>ExploreTex Sourcing Company Establish as a Garments Manufacturer, Buying or Sourcing Agent, Importer and Exporter to Sourcing Buyer from Abroad and Make a Good Deal with them to Fulfill Their Requirements. Our mission is to produce the best quality products and make long term business with our valued buyers. ExploreTex Sourcing Company doing work with reputed garments industry. We work with non Compliance, Compliance, BSCI, OEKO TEX, WRAP, CTPAT, WalMart 350 License Holder, etc Certified Factory. There are large numbers of expert Management and worker. They work in a state of the art factory environment. As a supplier, commitment and excellence is our topmost priority.</p>\r\n', 0, '2015-08-28 11:21:02'),
(11, 4, 7, 'Captain Haddock', 't-455854', 450, 50, 10, 'We advise you cold wash your garments and hang them out to dry. It’ll keep them super soft, looking their best and it’s even good for the environment.', '<p>Our classic Unisex T-Shirt is the staple of anyone’s wardrobe. A finely tailored men’s shirt constructed from 100% cotton, or a perfectly formed women’s shirt for those who don’t enjoy the more fitting nature of our Women’s T-Shirt. With a vast range of colors to choose from there’s no excuse for not having an arsenal of these ready for any eventuality.</p>\r\n', 0, '2015-08-31 02:07:37'),
(12, 4, 2, 'USPA Polo', 'p-95462', 490, 50, 10, 'A polo shirt, also known as a golf shirt and tennis shirt, is a form of shirt with a collar, a placket with typically two or three buttons, and an optional pocket. All three terms may be used interchangeably.', '<p>A polo shirt, also known as a golf shirt and tennis shirt, is a form of shirt with a collar, a placket with typically two or three buttons, and an optional pocket. All three terms may be used interchangeably. A polo shirt, also known as a golf shirt and tennis shirt, is a form of shirt with a collar, a placket with typically two or three buttons, and an optional pocket. All three terms may be used interchangeably.</p>\r\n', 0, '2015-08-31 02:16:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_image`
--

CREATE TABLE IF NOT EXISTS `tbl_product_image` (
`image_id` int(5) NOT NULL,
  `product_id` int(5) NOT NULL,
  `product_image_name` varchar(50) NOT NULL,
  `default_image` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_product_image`
--

INSERT INTO `tbl_product_image` (`image_id`, `product_id`, `product_image_name`, `default_image`) VALUES
(1, 1, 'images/product_image/pic1.jpg', 0),
(2, 1, 'images/product_image/smal3.jpg', 1),
(3, 1, 'images/product_image/smal.jpg', 1),
(4, 1, 'images/product_image/smal2.jpg', 1),
(5, 2, 'images/product_image/pic2.jpg', 0),
(6, 2, 'images/product_image/smal1.jpg', 1),
(7, 2, 'images/product_image/smal11.jpg', 1),
(8, 2, 'images/product_image/smal21.jpg', 1),
(9, 3, 'images/product_image/pic3.jpg', 0),
(10, 3, 'images/product_image/smal31.jpg', 1),
(11, 3, 'images/product_image/smal22.jpg', 1),
(12, 3, 'images/product_image/smal12.jpg', 1),
(13, 4, 'images/product_image/pic8.jpg', 0),
(14, 4, 'images/product_image/small4.jpg', 1),
(15, 4, 'images/product_image/smal32.jpg', 1),
(16, 4, 'images/product_image/smal23.jpg', 1),
(17, 5, 'images/product_image/s1.jpg', 0),
(18, 5, 'images/product_image/small41.jpg', 1),
(19, 5, 'images/product_image/smal24.jpg', 1),
(20, 5, 'images/product_image/smal13.jpg', 1),
(21, 6, 'images/product_image/pic10.jpg', 0),
(22, 6, 'images/product_image/smal33.jpg', 1),
(23, 6, 'images/product_image/smal34.jpg', 1),
(24, 6, 'images/product_image/small42.jpg', 1),
(30, 8, 'images/product_image/zarif11.jpg', 0),
(31, 8, 'images/product_image/zarif21.jpg', 1),
(32, 8, 'images/product_image/zarif31.jpg', 1),
(33, 8, 'images/product_image/zarif41.jpg', 1),
(34, 8, 'images/product_image/zarif51.jpg', 1),
(39, 10, 'images/product_image/031.jpg', 0),
(40, 10, 'images/product_image/021.jpg', 1),
(41, 10, 'images/product_image/download_(1)1.jpg', 1),
(42, 11, 'images/product_image/t-shirt2.jpg', 0),
(43, 11, 'images/product_image/t-shirt1.jpg', 1),
(44, 11, 'images/product_image/download_(3).jpg', 1),
(45, 11, 'images/product_image/download_(2).jpg', 1),
(46, 11, 'images/product_image/T-Shirt-Designs-012.jpg', 1),
(47, 12, 'images/product_image/polo4.jpg', 0),
(48, 12, 'images/product_image/polo.jpg', 1),
(49, 12, 'images/product_image/polo3.jpg', 1),
(50, 12, 'images/product_image/polo1.jpg', 1),
(51, 12, 'images/product_image/polo2.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_manufacturer`
--

CREATE TABLE IF NOT EXISTS `tbl_product_manufacturer` (
`manufacturer_id` int(5) NOT NULL,
  `manufacturer_name` varchar(80) NOT NULL,
  `manufacturer_description` text NOT NULL,
  `publication_status` tinyint(1) NOT NULL COMMENT 'if publication status==0 manufacturer published and if publication status==1 manufacturer unpublished.',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_product_manufacturer`
--

INSERT INTO `tbl_product_manufacturer` (`manufacturer_id`, `manufacturer_name`, `manufacturer_description`, `publication_status`, `created_date_time`) VALUES
(1, 'Hues Attire', 'It''s all about fashion dress.', 0, '2015-08-15 15:48:02'),
(2, 'Optional', 'Fashion and Design', 0, '2015-08-15 16:19:43'),
(3, 'Ethnic Designer Wear', 'It''s fashion design company. They are one of the most reputed Bangladeshi company. Specially they make women wear.', 0, '2015-08-19 03:51:24'),
(4, 'Riva Heavy Party Wear', 'It''s fashion design company. They are one of the most reputed Bangladeshi company. Specially they make women wear.', 0, '2015-08-19 03:51:53'),
(5, 'Fiza Heavy Party Weat', 'It''s fashion design company. They are one of the most reputed Bangladeshi company. Specially they make women wear.', 0, '2015-08-19 03:52:12'),
(6, 'Zarif Sharee & Kraft', 'it''s a Bangladeshi sharee brand ', 0, '2015-08-28 09:13:13'),
(7, 'Top Gear Bangladesh', 'It''s Bangladeshi company', 0, '2015-08-31 02:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_special_rate`
--

CREATE TABLE IF NOT EXISTS `tbl_product_special_rate` (
`product_special_id` int(5) NOT NULL,
  `product_id` int(5) NOT NULL,
  `product_special_price` int(5) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_product_special_rate`
--

INSERT INTO `tbl_product_special_rate` (`product_special_id`, `product_id`, `product_special_price`, `date_start`, `date_end`) VALUES
(1, 1, 250, '2015-08-01', '2015-08-22'),
(2, 3, 230, '2015-08-15', '2015-08-25'),
(3, 5, 199, '2015-08-25', '2015-09-05'),
(5, 8, 850, '2015-08-28', '2015-09-28'),
(7, 10, 900, '2015-08-29', '2015-09-30'),
(8, 11, 400, '2015-08-30', '2015-09-02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipping`
--

CREATE TABLE IF NOT EXISTS `tbl_shipping` (
`shipping_id` int(7) NOT NULL,
  `user_id` int(5) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `city` varchar(15) NOT NULL,
  `zip_code` int(5) NOT NULL,
  `country` varchar(20) NOT NULL,
  `state` varchar(20) NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_shipping`
--

INSERT INTO `tbl_shipping` (`shipping_id`, `user_id`, `full_name`, `email_address`, `mobile_no`, `city`, `zip_code`, `country`, `state`, `address`) VALUES
(1, 16, 'Rana Mia', 'rana@gmail.com', '016700000', 'Dhaka', 1349, 'Bangladesh', 'Dhaka', 'Dhaka'),
(2, 16, 'Rana Mia', 'rana@gmail.com', '016700000', 'Dhaka', 1349, 'Bangladesh', 'Dhaka', 'Dhaka'),
(3, 1, 'Afsana Rea', 'rea@gmail.com', '', '', 0, '', '', ''),
(4, 14, 'Black Khan', 'black@gmail.com', '0167100000', 'Dhaka', 1256, 'Bangladesh', 'Dhaka', 'Dhaka'),
(5, 17, 'Nasima Karim', 'nasima@gmail.com', '01719281489', 'Dhaka', 1349, 'Bangladesh', 'Dhaka', 'Dhaka, Bangladesh'),
(6, 14, 'Black Khan', 'black@gmail.com', '0167100000', 'Dhaka', 1256, 'Bangladesh', 'Dhaka', 'Dhaka'),
(7, 14, 'Black Khan', 'black@gmail.com', '0167100000', 'Dhaka', 1256, 'Bangladesh', 'Dhaka', 'Dhaka'),
(8, 15, 'ABC efg', 'abc@gmail.com', '0167000000', 'Dhaka', 1349, 'Bangladesh', 'Dhaka', 'Dhaka'),
(9, 1, 'Habibur Rahman', 'habib@gmail.com', '01671971440', 'Dhaka', 1349, 'Bangladesh', 'Dhaka', 'Ashulia, Dhaka'),
(10, 18, 'Habib', 'habib@gmail.com', '01671971440', 'Dhaka', 1349, 'Bangladesh', 'Dhaka', 'Ashulia, Dhaka'),
(11, 20, 'Yellow Khan', 'yellow@gmail.com', '0171000000', 'Dhaka', 1349, 'Bangladesh', 'Dhaka', 'Dhaka, Bangladesh'),
(12, 20, 'Yellow Khan', 'yellow@gmail.com', '0171000000', 'Dhaka', 1349, 'Bangladesh', 'Dhaka', 'Dhaka, Bangladesh'),
(13, 18, 'Naeem Ahmed', 'bdnaeem3@gmail.com', '017000000', 'Dhaka', 1349, 'Bangladesh', 'Dhaka', 'Dhaka, Bangladesh');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
`user_id` int(5) NOT NULL,
  `user_first_name` varchar(30) NOT NULL,
  `user_last_name` varchar(30) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `zip_code` varchar(5) NOT NULL,
  `city` varchar(15) NOT NULL,
  `country` varchar(20) NOT NULL,
  `state` varchar(15) NOT NULL,
  `newsletter` tinyint(1) NOT NULL,
  `user_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if user_status==0 user active and if user_status==1 user inactive',
  `block_customer` tinyint(1) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_first_name`, `user_last_name`, `user_name`, `user_email`, `user_password`, `mobile_no`, `address`, `zip_code`, `city`, `country`, `state`, `newsletter`, `user_status`, `block_customer`, `created_date_time`) VALUES
(1, 'Afsana', 'Rea', 'afsana.rea', 'rea@gmail.com', 'f83b2399cf42e7ab8947466f8b2ee447', '', '', '', '', '', '', 0, 0, 0, '2015-08-13 16:07:41'),
(6, 'Neel', 'Ahmed', 'neel', 'neel@gmail.com', 'c33367701511b4f6020ec61ded352059', '', '', '', '', '', '', 0, 0, 0, '2015-08-14 15:30:20'),
(12, 'Orange', 'Khan', 'orange', 'ami@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '017100000', 'Dhaka, Bangladesh', '1349', 'Dhaka', 'Bangladesh', 'Dhaka', 2, 0, 0, '2015-08-24 17:36:22'),
(13, 'Blue', 'Khan', 'blue', 'blue@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '017100000', 'Dhaka, Bangladesh', '1349', 'Dhaka', 'Bangladesh', 'Dhaka', 2, 0, 0, '2015-08-25 02:31:28'),
(14, 'Black', 'Khan', 'black', 'black@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '0167100000', 'Dhaka', '1256', 'Dhaka', 'Bangladesh', 'Dhaka', 1, 0, 0, '2015-08-25 04:16:41'),
(15, 'ABC', 'efg', 'abc', 'abc@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '0167000000', 'Dhaka', '1349', 'Dhaka', 'Bangladesh', 'Dhaka', 1, 0, 0, '2015-08-25 07:46:55'),
(16, 'Rana', 'Mia', 'rana', 'rana@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '016700000', 'Dhaka', '1349', 'Dhaka', 'Bangladesh', 'Dhaka', 1, 0, 0, '2015-08-25 13:01:26'),
(17, 'Nasima', 'Karim', 'karim', 'nasima@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '01719281489', 'Dhaka, Bangladesh', '1349', 'Dhaka', 'Bangladesh', 'Dhaka', 1, 0, 0, '2015-08-26 02:05:24'),
(18, 'Naeem', 'Ahmed', 'bdnaeem3', 'bdnaeem3@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '017000000', 'Dhaka, Bangladesh', '1349', 'Dhaka', 'Bangladesh', 'Dhaka', 1, 0, 0, '2015-08-28 06:17:39'),
(19, 'Green', 'Khan', 'green', 'green@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '016700000', 'Dhaka', '1349', 'Dhaka', 'Bangladesh', 'Dhaka', 1, 0, 0, '2015-08-28 06:48:44'),
(20, 'Yellow', 'Khan', 'yellow', 'yellow@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '0171000000', 'Dhaka, Bangladesh', '1349', 'Dhaka', 'Bangladesh', 'Dhaka', 1, 0, 0, '2015-08-29 03:03:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
 ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
 ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
 ADD PRIMARY KEY (`order_details_id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
 ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
 ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_product_image`
--
ALTER TABLE `tbl_product_image`
 ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `tbl_product_manufacturer`
--
ALTER TABLE `tbl_product_manufacturer`
 ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `tbl_product_special_rate`
--
ALTER TABLE `tbl_product_special_rate`
 ADD PRIMARY KEY (`product_special_id`);

--
-- Indexes for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
 ADD PRIMARY KEY (`shipping_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
MODIFY `admin_id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
MODIFY `category_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
MODIFY `order_id` int(7) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
MODIFY `order_details_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
MODIFY `payment_id` int(7) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
MODIFY `product_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_product_image`
--
ALTER TABLE `tbl_product_image`
MODIFY `image_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `tbl_product_manufacturer`
--
ALTER TABLE `tbl_product_manufacturer`
MODIFY `manufacturer_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_product_special_rate`
--
ALTER TABLE `tbl_product_special_rate`
MODIFY `product_special_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
MODIFY `shipping_id` int(7) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
