<?php

/**
 * Created by PhpStorm.
 * User: Habib
 * Date: 13-Aug-15
 * Time: 10:08 PM
 */
class Checkout extends CI_Controller
{

    public function index()
    {
        $data = array();
        $data['title'] = 'Login/Register';
        $data['user_id'] = $this->session->set_userdata('user_id');
        $data['maincontent'] = $this->load->view('register_view', $data, TRUE);
        $this->load->view('master', $data);
    }

    public function verified_email($user_email = NULL)
    {
        // echo $user_email;
        $result = $this->checkout_model->check_verified_email($user_email);
        if ($result) {
            echo 'Already Registered';
        } else {
            echo 'Available';
        }
    }

    public function check_login()
    {
        $ref = $this->input->get("ref");
        $user_email = $this->input->post('user_email', true);
        $user_password = $this->input->post('user_password', true);
        $user_status = $this->input->post('user_status', true);
        //echo $user_email.'-----'.$user_password.'----'.$user_status;
        // exit();
        $result = $this->user_login_model->check_login_info($user_email, $user_password, $user_status);
        // echo '<pre>';
        //print_r($result);

        $sdata = array();

        if ($result) {
            $sdata['user_first_name'] = $result->user_first_name;
            $sdata['user_last_name'] = $result->user_last_name;
            $sdata['user_id'] = $result->user_id;
            $sdata['user_status'] = $result->user_status;
            $this->session->set_userdata($sdata);
            if($ref=="")
            {
                redirect('welcome');
            } else
            {
                redirect($ref);
            }

        } else {
            $sdata['message'] = 'User ID or Password Invalid !';
            $this->session->set_userdata($sdata);
            redirect("checkout");
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_last_name');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_status');
        $this->cart->destroy();
        redirect('welcome', 'refresh');
    }

    public function register()
    {
        $data = array();
        $data['title'] = 'Register';
        $data['user_id'] = $this->session->userdata('user_id');
        $data['maincontent'] = $this->load->view('register_view', $data, TRUE);
        $this->load->view('master', $data);
    }

    public function save_user()
    {
        $data = array();
        $data['user_first_name'] = $this->input->post('user_first_name', true);
        $data['user_last_name'] = $this->input->post('user_last_name', true);
        $data['user_name'] = $this->input->post('user_name', true);
        $data['user_email'] = $this->input->post('user_email', true);
        $data['mobile_no'] = $this->input->post('mobile_no', true);
        $data['address'] = $this->input->post('address', true);
        $data['zip_code'] = $this->input->post('zip_code', true);
        $data['city'] = $this->input->post('city', true);
        $data['country'] = $this->input->post('country', true);
        $data['state'] = $this->input->post('state', true);
        $data['newsletter'] = $this->input->post('newsletter', true);
        $data['user_status'] = 0;
        $data['user_password'] = md5($this->input->post('user_password', true));
        $user_id = $this->general_model->save_info('tbl_user', $data);

        $sdata['user_id'] = $user_id;
        $sdata['user_last_name'] = $data['user_last_name'];
        $this->session->set_userdata($sdata);

        /*$sdata = array();
        $sdata['message'] = "Sign Up Successfully Completed.Please Log In Here !";
        $this->session->set_userdata($sdata);*/
        redirect('cart/show_cart');
    }

    public function shipping_same_as_billing()
    {
        $user_id=$this->session->userdata('user_id');
        $user_info=$this->checkout_model->select_user_by_id($user_id);

        $data=array();
        $data['user_id'] = $user_id;
        $data['full_name'] = $user_info->user_first_name.' '.$user_info->user_last_name;
        $data['email_address'] = $user_info->user_email;
        $data['mobile_no'] = $user_info->mobile_no;
        $data['city'] = $user_info->city;
        $data['country'] = $user_info->country;
        $data['zip_code'] = $user_info->zip_code;
        $data['state'] = $user_info->state;
        $data['address'] = $user_info->address;

        $shipping_id = $this->general_model->save_info('tbl_shipping', $data);
        $sdata['shipping_id'] = $shipping_id;
        $this->session->set_userdata($sdata);
        redirect('cart/show_cart');

    }
    public function save_shipping()
    {
        $data=array();
        $data['user_id']=$this->session->userdata('user_id');
        $data['full_name'] = $this->input->post('full_name',true);
        $data['email_address'] = $this->input->post('email_address',true);
        $data['mobile_no'] = $this->input->post('mobile_no',true);
        $data['city'] = $this->input->post('city',true);
        $data['zip_code'] = $this->input->post('zip_code',true);
        $data['country'] = $this->input->post('country',true);
        $data['state'] = $this->input->post('state',true);
        $data['address'] = $this->input->post('address',true);

        $shipping_id = $this->general_model->save_info('tbl_shipping', $data);
        $sdata['shipping_id'] = $shipping_id;
        $this->session->set_userdata($sdata);
        redirect('cart/show_cart');

    }
    public function confirm_order()
    {
        $payment_type = $this->input->post('payment_type');
        $sdata = array();
        if($payment_type == 'cash_on_delivery')
        {
            $data = array();
            $data['payment_type'] = $payment_type;
            $data['payment_status'] = 0;
            $payment_id=$this->general_model->save_info('tbl_payment',$data);
            $sdata['payment_id'] = $payment_id;
            $this->session->set_userdata($sdata);
            $this->checkout_model->save_order_info();
            $data=array();
            $data['title'] = 'Order Success';
            $data['maincontent'] = $this->load->view('order_successful',$data,true);
            $this->load->view('master',$data);
            $this->cart->destroy();
        }
        else
        {

        }

    }


} 