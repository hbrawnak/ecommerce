<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $title; ?></title>
    <link href="<?= base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css'/>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <!-- Ajax -->
    <script src="<?= base_url(); ?>js/ajax_utility.js"></script>
    <script src="<?= base_url(); ?>js/ajax_update.js"></script>

    <link href="<?= base_url(); ?>assets/css/component.css" rel='stylesheet' type='text/css'/>

    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!--webfont-->
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet'
          type='text/css'>
    <script src="<?= base_url(); ?>assets/js/jquery.easydropdown.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script src="<?= base_url(); ?>assets/js/jquery.magnific-popup.js" type="text/javascript"></script>
    <link href="<?= base_url(); ?>assets/css/magnific-popup.css" rel="stylesheet" type="text/css">
    <script>
        $(document).ready(function () {
            $('.popup-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });
        });
    </script>
    <!----details-product-slider--->
    <!-- Include the Etalage files -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/etalage.css">
    <script src="<?= base_url(); ?>assets/js/jquery.etalage.min.js"></script>
    <!-- Include the Etalage files -->
    <script>
        jQuery(document).ready(function ($) {

            $('#etalage').etalage({
                thumb_image_width: 300,
                thumb_image_height: 400,

                show_hint: true,
                click_callback: function (image_anchor, instance_id) {
                    alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');
                }
            });
            // This is for the dropdown list example:
            $('.dropdownlist').change(function () {
                etalage_show($(this).find('option:selected').attr('class'));
            });

        });
    </script>
    <!----//details-product-slider--->
    <script src="<?= base_url(); ?>assets/js/easyResponsiveTabs.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true   // 100% fit in a container
            });
        });
    </script>
    <link href="<?= base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css'/>
</head>
<body>
<div class="header">
<div class="container">
    <div class="header-top">
        <div class="logo">
            <a href="<?= base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt=""/></a>
        </div>
        <div class="header_right">
            <ul class="social">
                <li><a href=""> <i class="fb"> </i> </a></li>
                <li><a href=""><i class="tw"> </i> </a></li>
                <li><a href=""><i class="utube"> </i> </a></li>
                <li><a href=""><i class="pin"> </i> </a></li>
                <li><a href=""><i class="instagram"> </i> </a></li>
            </ul>
            <div class="lang_list">
                <select tabindex="4" class="dropdown">
                    <option value="" class="label" value="">En</option>
                    <option value="1">English</option>
                    <option value="2">French</option>
                    <option value="3">German</option>
                </select>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="index-banner">
        <div class="wmuSlider example1">
            <div class="wmuSliderWrapper">
                <article style="position: absolute; width: 100%; opacity: 0;">
                    <div class="banner-wrap">
                        <div class="bannertop_box">
                            <ul class="login">
                                <?php
                                $user_id = $this->session->userdata('user_id');
                                if ($user_id) {
                                    ?>
                                    <li class="login_text"><?php echo $this->session->userdata('user_last_name') . ' ' . '|'; ?></li>
                                    <li class="login_text"><a href="<?php echo base_url(); ?>checkout/logout">Log
                                            Out</a></li>
                                <?php
                                } else {
                                    ?>
                                    <li class="login_text"><a href="<?php echo base_url('checkout').'?ref='. current_uri(); ?>">Log In</a> / <a
                                            href="<?php echo base_url(); ?>welcome/register">Sign Up</a></li>
                                    <input type="hidden" value="1" name="login_status"/>
                                <?php
                                }
                                ?>
                                <li class="wish"><a href="<?= base_url(); ?>cart/add_to_cart">Wish List</a></li>
                                <div class='clearfix'></div>
                            </ul>
                            <div class="cart_bg">
                                <ul class="cart">
                                    <a href="<?= base_url(); ?>cart/add_to_cart""><i class="cart_icon"></i>

                                    <p class="cart_desc">$<?php echo $this->cart->total(); ?><br><span
                                            class="yellow"><?php echo $this->cart->total_items(); ?> items</span>
                                    </p></a>
                                    <div class='clearfix'></div>
                                </ul>
                                <ul class="product_control_buttons">
                                    <li><a href="<?= base_url('cart/show_cart'); ?>"><img src="<?php echo base_url(); ?>assets/images/close.png" alt=""/></a>
                                    </li>
                                    <li><a href="<?= base_url('cart/show_cart'); ?>">Edit</a></li>
                                </ul>
                                <div class='clearfix'></div>
                            </div>
                            <ul class="quick_access">
                                <li class="view_cart"><a href="<?= base_url(); ?>cart/show_cart">View Cart</a></li>
                                <li class="check"><a href="<?= base_url(); ?>cart/add_to_cart">Checkout</a></li>
                                <div class='clearfix'></div>
                            </ul>
                            <div class="search">
                                <input type="text" value="Search" onfocus="this.value = '';"
                                       onblur="if (this.value == '') {this.value = 'Search';}">
                                <input type="submit" value="">
                            </div>
                            <div class="welcome_box">
                                <h2>Welcome to Surfhouse</h2>

                                <p>Best idea never goes flop. Make idea and share with us for making you smile.</p>
                            </div>
                        </div>
                        <div class="banner_right">
                            <h1>Feugait<br> 2014</h1>

                            <p>Super easy going freeride boards based on the X-Cite Ride shape concept with additional
                                cpntrol and super easy jibing.</p>
                            <a href="<?= base_url('welcome/apparel'); ?>" class="banner_btn">Buy Now</a>
                        </div>
                        <div class='clearfix'></div>
                    </div>
                </article>
                <article style="position: absolute; width: 100%; opacity: 0;">
                    <div class="banner-wrap">
                        <div class="bannertop_box">
                            <ul class="login">
                                <?php
                                $user_id = $this->session->userdata('user_id');
                                if ($user_id) {
                                    ?>
                                    <li class="login_text"><?php echo $this->session->userdata('user_last_name') . ' ' . '|'; ?></li>
                                    <li class="login_text"><a href="<?php echo base_url(); ?>checkout/logout">Log
                                            Out</a></li>
                                <?php
                                } else {
                                    ?>
                                    <li class="login_text"><a href="<?php echo base_url('checkout').'?ref='. current_uri(); ?>">Log In</a> / <a
                                            href="<?php echo base_url(); ?>welcome/register">Sign Up</a></li>
                                <?php
                                }
                                ?>
                                <li class="wish"><a href="<?= base_url('checkout'); ?>">Wish List</a></li>
                                <div class='clearfix'></div>
                            </ul>
                            <div class="cart_bg">
                                <ul class="cart">
                                    <a href="<?= base_url('cart/show_cart'); ?>"><i class="cart_icon"></i>

                                    <p class="cart_desc">$<?php echo $this->cart->total(); ?><br><span
                                            class="yellow"><?php echo $this->cart->total_items(); ?> items</span>
                                    </p></a>
                                    <div class='clearfix'></div>
                                </ul>
                                <ul class="product_control_buttons">
                                    <li><a href="<?= base_url('cart/show_cart'); ?>"><img src="<?php echo base_url(); ?>assets/images/close.png" alt=""/></a>
                                    </li>
                                    <li><a href="<?= base_url('cart/show_cart'); ?>">Edit</a></li>
                                </ul>
                                <div class='clearfix'></div>
                            </div>
                            <ul class="quick_access">
                                <li class="view_cart"><a href="<?= base_url(); ?>cart/show_cart">View Cart</a></li>
                                <li class="check"><a href="<?=base_url('checkout');?>">Checkout</a></li>
                                <div class='clearfix'></div>
                            </ul>
                            <div class="search">
                                <input type="text" value="Search" onfocus="this.value = '';"
                                       onblur="if (this.value == '') {this.value = 'Search';}">
                                <input type="submit" value="">
                            </div>
                            <div class="welcome_box">
                                <h2>Welcome to Surfhouse</h2>

                                <p>We believe our product, that can make you satisfied.</p>
                            </div>
                        </div>
                        <div class="banner_right">
                            <h1>Mutation<br> 2014</h1>

                            <p>The onliy online store you will ever need for all your windsurfing and kitesurfing
                                and SUP nedds.</p>
                            <a href="<?= base_url('welcome/apparel'); ?>" class="banner_btn">Buy Now</a>
                        </div>
                        <div class='clearfix'></div>
                    </div>
                </article>
                <article style="position: absolute; width: 100%; opacity: 0;">
                    <div class="banner-wrap">
                        <div class="bannertop_box">
                            <ul class="login">
                                <?php
                                $user_id = $this->session->userdata('user_id');
                                if ($user_id) {
                                    ?>
                                    <li class="login_text"><?php echo $this->session->userdata('user_last_name') . ' ' . '|'; ?></li>
                                    <li class="login_text"><a href="<?php echo base_url(); ?>checkout/logout">Log
                                            Out</a></li>
                                <?php
                                } else {
                                    ?>
                                    <li class="login_text"><a href="<?php echo base_url('checkout').'?ref='. current_uri(); ?>">Log In</a> / <a
                                            href="<?php echo base_url(); ?>welcome/register">Sign Up</a></li>
                                <?php
                                }
                                ?>
                                <li class="wish"><a href="#">Wish List</a></li>
                                <div class='clearfix'></div>
                            </ul>
                            <div class="cart_bg">
                                <ul class="cart">
                                    <a href="<?= base_url(); ?>cart/show_cart"><i class="cart_icon"></i>

                                    <p class="cart_desc">$<?php echo $this->cart->total(); ?><br><span
                                            class="yellow"><?php echo $this->cart->total_items(); ?> items</span>
                                    </p></a>
                                    <div class='clearfix'></div>
                                </ul>
                                <ul class="product_control_buttons">
                                    <li><a href="<?= base_url('cart/show_cart'); ?>"><img src="<?php echo base_url(); ?>assets/images/close.png" alt=""/></a>
                                    </li>
                                    <li><a href="<?= base_url('cart/show_cart'); ?>">Edit</a></li>
                                </ul>
                                <div class='clearfix'></div>
                            </div>
                            <ul class="quick_access">
                                <li class="view_cart"><a href="<?= base_url(); ?>cart/show_cart">View Cart</a></li>
                                <li class="check"><a href="<?=base_url('checkout');?>">Checkout</a></li>
                                <div class='clearfix'></div>
                            </ul>
                            <div class="search">
                                <input type="text" value="Search" onfocus="this.value = '';"
                                       onblur="if (this.value == '') {this.value = 'Search';}">
                                <input type="submit" value="">
                            </div>
                            <div class="welcome_box">
                                <h2>Welcome to Surfhouse</h2>

                                <p>We want to see your smile face, that's why we made our best product for you.</p>
                            </div>
                        </div>
                        <div class="banner_right">
                            <h1>Jp Funride<br> 2014</h1>

                            <p>The onliy online store you will ever need for all your windsurfing and kitesurfing
                                and SUP nedds</p>
                            <a href="<?= base_url('welcome/apparel'); ?>" class="banner_btn">Buy Now</a>
                        </div>
                        <div class='clearfix'></div>
                    </div>
                </article>
            </div>
            <a class="wmuSliderPrev">Previous</a><a class="wmuSliderNext">Next</a>
            <ul class="wmuSliderPagination">
                <li><a href="#" class="">0</a></li>
                <li><a href="#" class="">1</a></li>
                <li><a href="#" class="wmuActive">2</a></li>
            </ul>
        </div>
        <script src="<?php echo base_url(); ?>assets/js/jquery.wmuSlider.js"></script>
        <script>
            $('.example1').wmuSlider();
        </script>
    </div>
</div>
</div>

<div class="main">
    <?php echo $maincontent; ?>
    <div class="container">
        <div class="brands">
            <ul class="brand_icons">
                <li><img src='<?php echo base_url(); ?>assets/images/icon1.jpg' class="img-responsive" alt=""/></li>
                <li><img src='<?php echo base_url(); ?>assets/images/icon2.jpg' class="img-responsive" alt=""/></li>
                <li><img src='<?php echo base_url(); ?>assets/images/icon3.jpg' class="img-responsive" alt=""/></li>
                <li><img src='<?php echo base_url(); ?>assets/images/icon4.jpg' class="img-responsive" alt=""/></li>
                <li><img src='<?php echo base_url(); ?>assets/images/icon5.jpg' class="img-responsive" alt=""/></li>
                <li><img src='<?php echo base_url(); ?>assets/images/icon6.jpg' class="img-responsive" alt=""/></li>
                <li class="last"><img src='<?php echo base_url(); ?>assets/images/icon7.jpg' class="img-responsive"
                                      alt=""/></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="instagram_top">
            <div class="instagram text-center">
                <h3><i class="insta_icon"> </i> Instagram feed:&nbsp;<span class="small">#Surfhouse</span></h3>
            </div>
            <ul class="instagram_grid">
                <li><a class="popup-with-zoom-anim" href="#small-dialog1"><img
                            src="<?php echo base_url(); ?>assets/images/i1.jpg" class="img-responsive" alt=""/></a></li>
                <li><a class="popup-with-zoom-anim" href="#small-dialog1"><img
                            src="<?php echo base_url(); ?>assets/images/i2.jpg" class="img-responsive" alt=""/></a></li>
                <li><a class="popup-with-zoom-anim" href="#small-dialog1"><img
                            src="<?php echo base_url(); ?>assets/images/i3.jpg" class="img-responsive" alt=""/></a></li>
                <li><a class="popup-with-zoom-anim" href="#small-dialog1"><img
                            src="<?php echo base_url(); ?>assets/images/i4.jpg" class="img-responsive" alt=""/></a></li>
                <li><a class="popup-with-zoom-anim" href="#small-dialog1"><img
                            src="<?php echo base_url(); ?>assets/images/i5.jpg" class="img-responsive" alt=""/></a></li>
                <li class="last_instagram"><a class="popup-with-zoom-anim" href="#small-dialog1"><img
                            src="<?php echo base_url(); ?>assets/images/i6.jpg" class="img-responsive" alt=""/></a></li>
                <div class="clearfix"></div>
                <div id="small-dialog1" class="mfp-hide">
                    <div class="pop_up">
                        <h4>A Sample Photo Stream</h4>
                        <img src="<?php echo base_url(); ?>assets/images/i_zoom.jpg" class="img-responsive" alt=""/>
                    </div>
                </div>
            </ul>
        </div>
        <ul class="footer_social">
            <li><a href="#"> <i class="fb"> </i> </a></li>
            <li><a href="#"><i class="tw"> </i> </a></li>
            <li><a href="#"><i class="pin"> </i> </a></li>
            <div class="clearfix"></div>
        </ul>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="footer-grid">
            <h3>Category</h3>
            <ul class="list1">
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Eshop</a></li>
                <li><a href="#">Features</a></li>
                <li><a href="#">New Collections</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </div>
        <div class="footer-grid">
            <h3>Our Account</h3>
            <ul class="list1">
                <li><a href="#">Your Account</a></li>
                <li><a href="#">Personal information</a></li>
                <li><a href="#">Addresses</a></li>
                <li><a href="#">Discount</a></li>
                <li><a href="#">Orders history</a></li>
                <li><a href="#">Addresses</a></li>
                <li><a href="#">Search Terms</a></li>
            </ul>
        </div>
        <div class="footer-grid">
            <h3>Our Support</h3>
            <ul class="list1">
                <li><a href="#">Site Map</a></li>
                <li><a href="#">Search Terms</a></li>
                <li><a href="#">Advanced Search</a></li>
                <li><a href="#">Mobile</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Mobile</a></li>
                <li><a href="#">Addresses</a></li>
            </ul>
        </div>
        <div class="footer-grid">
            <h3>Newsletter</h3>

            <p class="footer_desc">Nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo
                consequat</p>

            <div class="search_footer">
                <input type="text" class="text" value="Insert Email" onfocus="this.value = '';"
                       onblur="if (this.value == '') {this.value = 'Insert Email';}">
                <input type="submit" value="Submit">
            </div>
            <img src="<?php echo base_url(); ?>assets/images/payment.png" class="img-responsive" alt=""/>
        </div>
        <div class="footer-grid footer-grid_last">
            <h3>About Us</h3>

            <p class="footer_desc">Diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut
                wisi enim ad minim veniam,.</p>

            <p class="f_text">Phone: &nbsp;&nbsp;&nbsp;00-250-2131</p>

            <p class="email">Email: &nbsp;&nbsp;&nbsp;<span>info(at)Surfhouse.com</span></p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="footer_bottom">
    <div class="container">
        <div class="copy">
            <p>&copy; 2014 Template by <a href="http://w3layouts.com" target="_blank"> w3layouts</a></p>
        </div>
    </div>
</div>
</body>
</html>