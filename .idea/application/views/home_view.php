<div class="content_box">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="menu_box">
                    <h3 class="menu_head">Menu</h3>
                    <ul class="nav">
                        <li><a href="<?= base_url(); ?>">Home</a></li>
                        <li><a href="<?= base_url(); ?>welcome/apparel">Apparel</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="<?= base_url(); ?>welcome/contact">Contact</a></li>
                    </ul>
                </div>
                <div class="side_banner">
                    <div class="banner_img"><img src="<?php echo base_url(); ?>assets/images/pic9.jpg"
                                                 class="img-responsive" alt=""/></div>
                    <div class="banner_holder">
                        <h3>Now <br> is <br> Open!</h3>
                    </div>
                </div>
                <div class="tags">
                    <h4 class="tag_head">Tags Widget</h4>
                    <ul class="tags_links">
                        <li><a href="#">Kitesurf</a></li>
                        <li><a href="#">Super</a></li>
                        <li><a href="#">Duper</a></li>
                        <li><a href="#">Theme</a></li>
                        <li><a href="#">Men</a></li>
                        <li><a href="#">Women</a></li>
                        <li><a href="#">Equipment</a></li>
                        <li><a href="#">Best</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Men</a></li>
                        <li><a href="#">Apparel</a></li>
                        <li><a href="#">Super</a></li>
                        <li><a href="#">Duper</a></li>
                        <li><a href="#">Theme</a></li>
                        <li><a href="#">Responsiv</a></li>
                        <li><a href="#">Women</a></li>
                        <li><a href="#">Equipment</a></li>
                    </ul>
                    <a href="#" class="link1">View all tags</a>
                </div>
                <div class="tags">
                    <h4 class="tag_head">Articles Experts</h4>
                    <ul class="article_links">
                        <li><a href="#">Eleifend option congue nihil</a></li>
                        <li><a href="#">Investigationes demonst</a></li>
                        <li><a href="#">Qui sequitur mutationem</a></li>
                        <li><a href="#">videntur parum clar sollemnes</a></li>
                        <li><a href="#">ullamcorper suscipit lobortis</a></li>
                        <li><a href="#">commodo consequat. Duis autem</a></li>
                        <li><a href="#">Investigationes demonst</a></li>
                        <li><a href="#">ullamcorper suscipit lobortis</a></li>
                        <li><a href="#">Qui sequitur mutationem</a></li>
                        <li><a href="#">videntur parum clar sollemnes</a></li>
                        <li><a href="#">ullamcorper suscipit lobortis</a></li>
                    </ul>
                    <a href="#" class="link1">View all</a>
                </div>
            </div>
            <div class="col-md-9">
                <h3 class="m_1">New Products</h3>

                <div class="content_grid">

                    <?php
                    foreach ($all_published_product as $product) {
                        ?>
                        <div class="col_1_of_3 span_1_of_3">
                            <div class="view view-first">
                                <a href="<?= base_url(); ?>welcome/details/<?php echo $product->product_id; ?>">
                                    <div class="inner_content clearfix">
                                        <div class="product_image">
                                            <img
                                                src="<?php echo base_url() ?><?php echo $product->product_image_name ?>"
                                                class="img-responsive" alt=""/>

                                            <div class="mask">
                                                <div class="info">View Detail</div>
                                            </div>
                                            <div class="product_container">
                                                <div class="cart-left">
                                                    <p class="title"><?= $product->product_name; ?></p>
                                                </div>
                                                <?php
                                                $today = strtotime(date("Y-m-d"));
                                                $start_date = strtotime($product->date_start);
                                                $end_date = strtotime($product->date_end);
                                                if (($product->product_special_price) && (($today >= $start_date) && ($today <= $end_date))) {
                                                    ?>
                                                    <div class="price"><?= $product->product_special_price; ?></div>
                                                    <div class="reducedprice"><?= $product->product_price; ?></div>
                                                <?php
                                                } else {
                                                    ?>
                                                    <div class="price"><?= $product->product_price; ?></div>
                                                <?php
                                                }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php
                                        if ((($today >= $start_date) && ($today <= $end_date))) {
                                            ?>
                                            <div class="sale-box"><span class="on_sale title_shop">Sale</span></div>
                                        <?php
                                        } else {
                                            ?>
                                            <div class="sale-box1"><span class="on_sale1 title_shop">New</span></div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>


                <h3 class="m_2">Sale Products</h3>

                <div class="content_grid">
                    <?php
                    foreach ($all_published_product as $product) {
                        $today = strtotime(date("Y-m-d"));
                        $start_date = strtotime($product->date_start);
                        $end_date = strtotime($product->date_end);
                        if (($product->product_special_price) && (($today >= $start_date) && ($today <= $end_date))) {
                            ?>
                            <div class="col_1_of_3 span_1_of_3">
                                <div class="view view-first">
                                    <a href="<?= base_url(); ?>welcome/details/<?php echo $product->product_id; ?>">
                                        <div class="inner_content clearfix">
                                            <div class="product_image">
                                                <img
                                                    src="<?php echo base_url() ?><?php echo $product->product_image_name ?>"
                                                    class="img-responsive" alt=""/>

                                                <div class="mask">
                                                    <div class="info">View Detail</div>
                                                </div>
                                                <div class="product_container">
                                                    <div class="cart-left">
                                                        <p class="title"><?= $product->product_name; ?></p>
                                                    </div>
                                                    <?php
                                                    if (($product->product_special_price) && (($today >= $start_date) && ($today <= $end_date))) {
                                                        ?>
                                                        <div class="price"><?= $product->product_special_price; ?></div>
                                                        <div class="reducedprice"><?= $product->product_price; ?></div>
                                                    <?php
                                                    } else {
                                                        ?>
                                                        <div class="price"><?= $product->product_price; ?></div>
                                                    <?php
                                                    }
                                                    ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <?php
                                            if ((($today >= $start_date) && ($today <= $end_date))) {
                                                ?>
                                                <div class="sale-box"><span class="on_sale title_shop">Sale</span></div>
                                            <?php
                                            } else {
                                                ?>
                                                <div class="sale-box1"><span class="on_sale1 title_shop">New</span>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php
                        }
                    }
                    ?>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>