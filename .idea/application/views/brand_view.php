<div class="content_box">
<div class="container">
<div class="row">
<div class="col-md-3">
    <div class="menu_box">
        <h3 class="menu_head">Menu</h3>
        <ul class="nav">
            <li><a href="<?= base_url(); ?>">Home</a></li>
            <li><a href="<?= base_url(); ?>welcome/apparel">Apparel</a></li>
            <li><a href="#">About</a></li>
            <li><a href="<?= base_url(); ?>welcome/contact">Contact</a></li>
        </ul>
    </div>
    <div class="category">
        <h3 class="menu_head">Category Options</h3>
        <?php
        foreach ($all_published_category as $v_category) {
            ?>
            <ul class="category_nav">
                <li><a href="<?php echo base_url('welcome/category');?>/<?php echo $v_category->category_id;?>"><?php echo $v_category->category_name; ?></a></li>

            </ul>
        <?php } ?>
    </div>
    <div class="category">
        <h3 class="menu_head">Brand Options</h3>
        <?php
        foreach ($all_published_manufacturer as $v_brand) {
            ?>
            <ul class="category_nav">
                <li><a href="<?php echo base_url('welcome/manufacturer');?>/<?php echo $v_brand->manufacturer_id;?>"><?php echo $v_brand->manufacturer_name; ?></a></li>
            </ul>
        <?php
        }
        ?>
    </div>
    <div class="tags">
        <h4 class="tag_head">Tags Widget</h4>
        <ul class="tags_links">
            <li><a href="#">Kitesurf</a></li>
            <li><a href="#">Super</a></li>
            <li><a href="#">Duper</a></li>
            <li><a href="#">Theme</a></li>
            <li><a href="#">Men</a></li>
            <li><a href="#">Women</a></li>
            <li><a href="#">Equipment</a></li>
            <li><a href="#">Best</a></li>
            <li><a href="#">Accessories</a></li>
            <li><a href="#">Men</a></li>
            <li><a href="#">Apparel</a></li>
            <li><a href="#">Super</a></li>
            <li><a href="#">Duper</a></li>
            <li><a href="#">Theme</a></li>
            <li><a href="#">Responsiv</a></li>
            <li><a href="#">Women</a></li>
            <li><a href="#">Equipment</a></li>
        </ul>
        <a href="#" class="link1">View all tags</a>
    </div>
    <div class="side_banner">
        <div class="banner_img"><img src="<?php echo base_url(); ?>assets/images/pic9.jpg"
                                     class="img-responsive"
                                     alt=""/></div>
        <div class="banner_holder">
            <h3>Now <br> is <br> Open!</h3>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="dreamcrub">
        <ul class="breadcrumbs">
            <li class="home">
                <a href="index.html" title="Go to Home Page">Home</a>&nbsp;
                <span>&gt;</span>
            </li>
            <li class="home">&nbsp;
                Apparel&nbsp;
                <span>&gt;</span>&nbsp;
            </li>
            <li class="women">
                Women
            </li>
        </ul>
        <ul class="previous">
            <li><a href="index.html">Back to Previous Page</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="mens-toolbar">
        <div class="sort">
            <div class="sort-by">
                <label>Sort By</label>
                <select>
                    <option value="">
                        Position
                    </option>
                    <option value="">
                        Name
                    </option>
                    <option value="">
                        Price
                    </option>
                </select>
                <a href=""><img src="<?php echo base_url(); ?>assets/images/arrow2.gif" alt=""
                                class="v-middle"></a>
            </div>
        </div>
        <ul class="women_pagenation dc_paginationA dc_paginationA06">
            <li><a href="#" class="previous">Page:</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
        <div class="cbp-vm-options">
            <a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid"
               title="grid">Grid View</a>
            <a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list" title="list">List
                View</a>
        </div>
        <div class="pages">
            <div class="limiter visible-desktop">
                <label>Show</label>
                <select>
                    <option value="" selected="selected">
                        9
                    </option>
                    <option value="">
                        15
                    </option>
                    <option value="">
                        30
                    </option>
                </select> per page
            </div>
        </div>
        <div class="clearfix"></div>
        <ul>
            <?php
            foreach ($all_published_product_by_manufacturer as $product) {
                ?>

                <li>
                    <a class="cbp-vm-image"
                       href="<?= base_url(); ?>welcome/details/<?php echo $product->product_id; ?>">
                        <div class="view view-first">
                            <div class="inner_content clearfix">
                                <div class="product_image">
                                    <img
                                        src="<?php echo base_url() ?><?php echo $product->product_image_name ?>"
                                        class="img-responsive" alt=""/>

                                    <div class="mask">
                                        <div class="info">View Detail</div>
                                    </div>
                                    <div class="product_container">
                                        <div class="cart-left">
                                            <p class="title"><?= $product->product_name; ?></p>
                                        </div>
                                        <?php
                                        $today = strtotime(date("Y-m-d"));
                                        $start_date = strtotime($product->date_start);
                                        $end_date = strtotime($product->date_end);
                                        if (($product->product_special_price) && (($today >= $start_date) && ($today <= $end_date))) {
                                            ?>
                                            <div class="price"><?= $product->product_special_price; ?></div>
                                            <div class="reducedprice"><?= $product->product_price; ?></div>
                                        <?php
                                        } else {
                                            ?>
                                            <div class="price"><?= $product->product_price; ?></div>
                                        <?php
                                        }
                                        ?>



                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>


                    <a class="cbp-vm-icon cbp-vm-add" title="Add to cart"
                       href="<?= base_url(); ?>welcome/details/<?php echo $product->product_id; ?>">Add to
                        cart</a>
                </li>
            <?php
            }
            ?>


        </ul>
    </div>
    <script src="js/cbpViewModeSwitch.js" type="text/javascript"></script>
    <script src="js/classie.js" type="text/javascript"></script>
</div>
</div>
</div>
</div>