<div class="container">
    <div class="register">
        <form method="post" action="<?php echo base_url();?>checkout/save_shipping">
            <div class="register-top-grid">
                <h3>Shipping Detail</h3>
                <a href="<?php echo base_url();?>checkout/shipping_same_as_billing"><h3 align="right">Shipping Same As Billing</h3></a>
                <div>
                    <span>Full Name<label>*</label></span>
                    <input type="text" name="full_name" placeholder="Full Name" required="">
                </div>
                <div>
                    <span>Email Address<label>*</label></span>
                    <input type="email" name="email_address" onblur="makerequest(this.value,'res');" placeholder="Email Address" required=""><span class="res" id="res"></span>
                </div>
                <div>
                    <span>Mobile No<label>*</label></span>
                    <input type="text" name="mobile_no" placeholder="Mobile No" required="">
                </div>
                <div>
                    <span>City<label>*</label></span>
                    <input type="text" name="city" placeholder="City" required="">
                </div>
                <div>
                    <span>Zip Code<label>*</label></span>
                    <input type="text" name="zip_code" placeholder="Zip Code" required="">
                </div>
                <div>
                    <span>Country<label>*</label></span>
                    <input type="text" name="country" placeholder="Country" required="">
                </div>
                <div>
                    <span>State<label>*</label></span>
                    <input type="text" name="state" placeholder="State" required="">
                </div>
                <div>
                    <span>Address<label>*</label></span>
                    <textarea placeholder="Address" cols="62" name="address"></textarea>
                </div>
            </div>

            <div class="register-but">
                <div class="clearfix"></div>
                <input id="register_btn" type="submit" value="continue"><br/><br/>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>