<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="fa fa-users">
            Add Category
            <small>Product Category</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Product</a></li>
            <li class="active">Add Category</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="form-group">
                        <?php
                        $message=$this->session->userdata('message');
                        if($message)
                        {
                            ?>
                            <div class="alert alert-success"><p>
                                    <?php echo $message; ?>
                                </p>
                            </div>
                            <?php
                            $this->session->unset_userdata('message');
                        }
                        ?>
                    </div>

                    <div class="box-header with-border">
                        <h3 class="box-title">Please fill up the form...</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="<?php echo base_url();?>super_admin/save_category">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Category Name</label>
                                <input type="text" class="form-control" name="category_name" id="exampleInputEmail1" placeholder="Category Name" required="">
                            </div>
                            <div class="form-group">
                                <label>Category Description</label>
                                <textarea class="form-control" name="category_description" rows="3" placeholder="Enter a description ..." required=""></textarea>
                            </div>
                            <div class="form-group">
                                <div><label>Publication Status</label></div>
                                <label>
                                    <input type="radio" name="publication_status" value="0" class="minimal" checked /> <small>Published</small>
                                </label>
                                <br>
                                <label>
                                    <input type="radio" name="publication_status" value="1" class="minimal" /> <small>Unpublished</small>
                                </label>
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Add Category</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </div>
                    </form>
                </div><!-- /.box -->
            </div><!--/.col (left) -->
            <!-- right column -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->