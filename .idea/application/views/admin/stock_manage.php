<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="fa fa-users">
            Manage Product Stock
            <small>Product Stock Data Tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Product</a></li>
            <li class="active">Manage Product Stock</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manage Product Stock Data Table</h3>
                        <div class="form-group">
                            <?php
                            $message=$this->session->userdata('message');
                            if($message)
                            {
                                ?>
                                <div align="center" class="alert alert-success"><p>
                                        <?php echo $message; ?>
                                    </p>
                                </div>
                                <?php
                                $this->session->unset_userdata('message');
                            }
                            ?>
                        </div>

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Product Id</th>
                                <th>Product Image</th>
                                <th>Product Name</th>
                                <th>Product Quantity</th>
                                <th>Product Re-Order Level</th>
                                <th>Product Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($all_product as $v_product) {
                                ?>
                                <tr>
                                    <td><?php echo $v_product->product_id;?></td>
                                    <td><img height="50" width="50" src="<?php echo base_url() ?><?php echo $v_product->product_image_name; ?>"></td>
                                    <td><?php echo $v_product->product_name;?></td>
                                    <td><?php echo $v_product->product_quntity;?></td>
                                    <td><?php echo $v_product->product_reorder_level;?></td>
                                    <td>
                                        <?php
                                        if($v_product->publication_status == 0)
                                        {
                                            echo 'Published';
                                        }
                                        else
                                        {
                                            echo 'Unpublished';
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <form method="post" action="<?php echo base_url();?>super_admin/update_product_quantity/<?php echo $v_product->product_id;?>">
                                            <input type="number" class="form-control" name="product_quntity"
                                                   id="exampleInputEmail1" placeholder="Add Product Quantity">
                                            <button type="submit" class="btn bg-purple margin"
                                                    title="Add Product Quantity">Add
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->