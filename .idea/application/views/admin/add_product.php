<script language="JavaScript">
    var i = 1;
    function changeIt()
    {
        if(i>4)
        {
            alert("maximum 5 images you can select.")
        }
        else
        {
            my_div.innerHTML = my_div.innerHTML +"<br><input type='file' name='product_image_name[]'>"
        }
        i++;

        s('[data-toggle=tab').click(function(){
            if($(this).parent().hasClass('active'))
            {
                $($(this).attr("href")).toggleClass('active');
            }
        })
    }
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="fa fa-users">
            Add Product
            <small>Product Details</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Product</a></li>
            <li class="active">Add Product</li>
        </ol>
    </section>
    <div class="form-group">
        <?php
        $message=$this->session->userdata('message');
        if($message)
        {
            ?>
            <div class="alert alert-success"><p>
                    <?php echo $message; ?>
                </p>
            </div>
            <?php
            $this->session->unset_userdata('message');
        }
        ?>
    </div>
    <!-- Main content -->
    <section class="content">
        <form role="form" enctype="multipart/form-data" method="post" action="<?=base_url();?>super_admin/save_product">
        <div class="row">
            <div class="box-footer" align="center">
                <button type="submit" class="btn btn-success">Add Product</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </div>
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary" style="float: left">
                    <div class="box-header with-border">
                        <h3 class="box-title">Please fill up the form...</h3>
                    </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product Name</label>
                                <input type="text" class="form-control" name="product_name" id="exampleInputEmail1" placeholder="Product Name" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product Model</label>
                                <input type="text" class="form-control" name="product_model" id="exampleInputEmail1" placeholder="Product Model" required="">
                            </div>
                            <div class="form-group">
                                <label>Select Manufacturer</label>
                                <select name="manufacturer_id" class="form-control select2">
                                    <option selected="selected">Select...</option>
                                    <?php
                                    foreach($all_published_manufacturer as $v_manufacturer)
                                    {
                                        ?>
                                        <option value="<?php echo $v_manufacturer->manufacturer_id?>"><?php echo $v_manufacturer->manufacturer_name?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product Price</label>
                                <input type="text" class="form-control" name="product_price" id="exampleInputEmail1" placeholder="Product Price" required="">
                            </div>
                            <div class="form-group">
                                <label>Select Category</label>
                                <select name="category_id" class="form-control select2">
                                    <option selected="selected">Select...</option>
                                    <?php
                                    foreach($all_published_category as $v_category)
                                    {
                                        ?>
                                        <option value="<?php echo $v_category->category_id?>"><?php echo $v_category->category_name?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Product Short Description</label>
                                <textarea class="form-control" name="product_short_description" rows="3" placeholder="Enter a description ..." required=""></textarea>
                            </div>
                            <div class="form-group">
                                <label>Product long Description</label>
                                <textarea id="ck_editor" class="form-control" name="product_long_description" rows="3" required=""></textarea><?php echo display_ckeditor($editor['ckeditor']);?>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product Quantity</label>
                                <input type="text" class="form-control" name="product_quntity" id="exampleInputEmail1" placeholder="Product Quantity" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product Reorder Level</label>
                                <input type="text" class="form-control" name="product_reorder_level" id="exampleInputEmail1" placeholder="Product Reorder">
                            </div>
                            <div class="form-group">
                                <div><label>Publication Status</label></div>
                                <label>
                                    <input type="radio" name="publication_status" value="0" class="minimal" checked /> <small>Published</small>
                                </label>
                                <br>
                                <label>
                                    <input type="radio" name="publication_status" value="1" class="minimal" /> <small>Unpublished</small>
                                </label>
                            </div>
                        </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>


            <div class="col-md-6">
                <div class="box box-info" style="float: left">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Images</h3>
                        <p class="help-block">You can select up to five (5) images.</p>
                    </div>
                    <div class="box-header with-border">
                        <button type="button" onclick="changeIt()">Add More Images</button>
                    </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputFile">File input</label><br>
                                <input name="default_image" type="radio" value="0" id="exampleInputFile" checked>Default Image
                                <input name="product_image_name[]" type="file" id="exampleInputFile">
                            </div>
                            <div class="form-group" id="my_div">

                            </div>
                        </div><!-- /.box-body -->
                </div>
                <div class="box box-info" style="float: left">
                    <div class="box-header with-border">
                        <h3 class="box-title">Special Offer</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Start Date</label>
                                <input type="date" class="form-control" name="date_start" id="exampleInputEmail1">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">End Date</label>
                                <input type="date" class="form-control" name="date_end" id="exampleInputEmail1">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Special Product Price</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-dollar"></i>
                                    </div>
                                    <input type="text" name="product_special_price" class="form-control" placeholder="Special Product Price"/>
                                </div><!-- /.input group -->
                            </div>
                        </div><!-- /.box-body -->
                </div>
            </div><!--/.col (left) -->
            <!-- right column -->
        </div>   <!-- /.row -->
        </form>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->