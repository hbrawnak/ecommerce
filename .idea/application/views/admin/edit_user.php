<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="fa fa-edit">
            Edit User
            <small>Normal User</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User</a></li>
            <li><a href="#">Add User</a></li>
            <li class="active">Edit User</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">

                    <div class="box-header with-border">
                        <h3 class="box-title">Please update the form...</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="<?php echo base_url();?>super_admin/update_user/<?php echo $user_info->user_id?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">First Name</label>
                                <input type="text" class="form-control" name="user_first_name" value="<?php echo $user_info->user_first_name;?>" id="exampleInputEmail1" placeholder="First Name" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Last Name</label>
                                <input type="text" class="form-control" name="user_last_name" value="<?php echo $user_info->user_last_name;?>" id="exampleInputEmail1" placeholder="Last Name" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">User Name</label>
                                <input type="text" class="form-control" name="user_name" value="<?php echo $user_info->user_name;?>" id="exampleInputEmail1" placeholder="User Name" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" name="user_email" value="<?php echo $user_info->user_email;?>" id="exampleInputEmail1" placeholder="Enter email" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Update Password</label>
                                <input type="password" class="form-control" name="user_password" id="exampleInputPassword1" placeholder="Update Password" required="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Confirm Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password" required="">
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success">Update User</button>
                            <button type="reset" class="btn btn-danger">Reset</button>

                    </form>
                    <form method="post" action="<?php echo base_url();?>super_admin/manage_user">
                        <button align="center" style="margin-left: 178px;margin-top: -56px;" type="submit" class="btn btn-default">Cancel</button>
                    </form>
                        </div>
                </div><!-- /.box -->
            </div><!--/.col (left) -->
            <!-- right column -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->