<?php
/**
 * Created by PhpStorm.
 * User: Habib
 * Date: 06-Aug-15
 * Time: 9:22 PM
 */

class Admin_Login_Model extends CI_Model {

    public function check_login_info($admin_email_address,$admin_password)
    {
        $this->db->select('*');
        $this->db->from('tbl_admin');
        $this->db->where('admin_email_address',$admin_email_address);
        $this->db->where('admin_password',md5($admin_password));

        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
} 