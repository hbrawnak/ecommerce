/**
 * Created by Habib on 24-Aug-15.
 */
var xmlhttp = false;
try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
}
catch (e) {
    try {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
        xmlhttp = false;
    }
}

if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    xmlhttp = new XMLHttpRequest();
    //alert("You are not using Internet Explorer");
}

function makerequest(user_email, objID) {
    //var given_text = document.getElementById('given_text').value;
    if (user_email) {
        serverPage = 'checkout/verified_email/' + user_email;
        //alert(serverPage);
        xmlhttp.open("GET", serverPage);
        xmlhttp.onreadystatechange = function () {
            //alert(xmlhttp.readyState);
            //alert(xmlhttp.status);
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById(objID).innerHTML = xmlhttp.responseText;
                var text = xmlhttp.responseText;
                if (text == 'Already Registered') {
                    document.getElementById('register_btn').disabled = true;
                }
                if (text == 'Available') {
                    document.getElementById('register_btn').disabled = false;
                }
            }
        }
    }
    xmlhttp.send(null);
}